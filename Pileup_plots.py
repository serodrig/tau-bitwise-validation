#!/usr/bin/env python3                                                                                                                                                            
# -*- coding: utf-8 -*-                                                                                                                                                               
import os,sys
import ROOT
ROOT.gROOT.SetBatch(True)
from array import array
import inspect
import random                                                                                                                                   
#import StringIO                                                                                                                                     
from ROOT import gROOT
import json

from ROOT import TTree, TChain, TF1, TH1I, TH1F, TH1D, TH2F, TH2D, TGraphErrors, TGraph, THStack, TLegend, TGraph2D, TLatex
from ROOT import TFile, TCanvas, TPad, TLine, TLorentzVector
from ROOT import gPad,gStyle

gStyle.SetNumberContours(255);                                                     

def plotting(_file_,_dir_):
    
    print("Opening file..")
    rootFile = TFile(_file_)
    tree = rootFile.Get("data")

    v_FPGAid        = ROOT.vector('int')()
    v_jFEXid        = ROOT.vector('int')()
    v_rho_EM        = ROOT.vector('int')()
    v_rho_HAD1      = ROOT.vector('int')()
    v_rho_HAD2      = ROOT.vector('int')()
    v_rho_HAD3      = ROOT.vector('int')()
    v_rho_FCAL      = ROOT.vector('int')()
    v_map_ID        = ROOT.vector(ROOT.vector('int'))()
    v_et_had_jet    = ROOT.vector(ROOT.vector('int'))()
    v_et_em_jet     = ROOT.vector(ROOT.vector('int'))()
    v_et_total_jet  = ROOT.vector(ROOT.vector('int'))()
    v_et_had_met    = ROOT.vector(ROOT.vector('int'))()
    v_et_em_met     = ROOT.vector(ROOT.vector('int'))()
    v_et_total_met  = ROOT.vector(ROOT.vector('int'))()

    print("Setting up branches..")
    tree.SetBranchStatus('*',0)
    tree.SetBranchStatus('pileup_*',1)
    tree.SetBranchAddress("pileup_FPGAid"                   , v_FPGAid      )
    tree.SetBranchAddress("pileup_jFEXid"                   , v_jFEXid      )
    tree.SetBranchAddress("pileup_rho_EM"                   , v_rho_EM      )
    tree.SetBranchAddress("pileup_rho_HAD1"                 , v_rho_HAD1    )
    tree.SetBranchAddress("pileup_rho_HAD2"                 , v_rho_HAD2    )   
    tree.SetBranchAddress("pileup_rho_HAD3"                 , v_rho_HAD3    )
    tree.SetBranchAddress("pileup_rho_FCAL"                 , v_rho_FCAL    )
    tree.SetBranchAddress("pileup_map_ID"                   , v_map_ID      )
    tree.SetBranchAddress("pileup_map_Et_values_HAD_jet"    , v_et_had_jet  )
    tree.SetBranchAddress("pileup_map_Et_values_EM_jet"     , v_et_em_jet   )
    tree.SetBranchAddress("pileup_map_Et_values_Total_jet"  , v_et_total_jet)
    tree.SetBranchAddress("pileup_map_Et_values_HAD_met"    , v_et_had_met  )
    tree.SetBranchAddress("pileup_map_Et_values_EM_met"     , v_et_em_met   )
    tree.SetBranchAddress("pileup_map_Et_values_Total_met"  , v_et_total_met)
    

    print("Creating histos..")
    h_rho_em        = TH1F( 'rho EM'    , 'rho EM'          , 200, 0, 1 )
    h_rho_had1      = TH1F( 'rho HAD1'  , 'rho HAD1'        , 200, 0, 1 )
    h_rho_had2      = TH1F( 'rho HAD2'  , 'rho HAD2'        , 200, 0, 1 )
    h_rho_had3      = TH1F( 'rho HAD3'  , 'rho HAD3'        , 200, 0, 1 )
    h_rho_fcal      = TH1F( 'rho FCAL'  , 'rho FCAL'        , 200, 0, 1 )
    h_et_had_jet    = TH1F( 'et_had_jet', 'et_had_jet'      , 302, -2, 300 )
    h_et_em_jet     = TH1F( 'et_em_jet' , 'et_em_jet'       , 302, -2, 300 )
    h_et_total_jet  = TH1F( 'et_total_jet'  , 'et_total_jet', 302, -2, 300 )
    h_et_had_met    = TH1F( 'et_had_met'    , 'et_had_met'  , 302, -2, 300 )
    h_et_em_met     = TH1F( 'et_em_met'     , 'et_em_met'   , 302, -2, 300 )
    h_et_total_met  = TH1F( 'et_total_met'  , 'et_total_met', 302, -2, 300 )

    entries = tree.GetEntries()
    # entries = 10
    
    for i in range(entries):
        if(((i+1)%100)==0): print("Events: ",i+1)
        tree.GetEntry(i)
        if not (v_rho_EM.size()==v_rho_HAD1.size()==v_rho_HAD2.size()==v_rho_HAD3.size()==v_rho_FCAL.size()):
            exit("ERROR: Rho vectors with different size..")
        if not (v_map_ID.size()==v_et_had_jet.size()==v_et_em_jet.size()==v_et_total_jet.size()==v_et_had_met.size()==v_et_em_met.size()==v_et_total_met.size()):
            exit("ERROR: Et vectors with different size..")
            
        for FPGA in range(v_rho_EM.size()):
            h_rho_em.Fill(v_rho_EM[FPGA]/1.e3)
            h_rho_had1.Fill(v_rho_HAD1[FPGA]/1.e3)
            h_rho_had2.Fill(v_rho_HAD2[FPGA]/1.e3)
            h_rho_had3.Fill(v_rho_HAD3[FPGA]/1.e3)
            h_rho_fcal.Fill(v_rho_FCAL[FPGA]/1.e3)
        
        
        for FPGA in range(v_map_ID.size()):
            for TT in range(v_map_ID[FPGA].size()):
                # if(v_et_total_jet[FPGA][TT]!=0): print(format(v_map_ID[FPGA][TT],"11d"),"----->",format(v_et_had_jet[FPGA][TT],"7d"),format(v_et_em_jet[FPGA][TT],"7d"),format(v_et_total_jet[FPGA][TT],"7d"))
            
                # if(v_map_ID[FPGA][TT]>=601024): continue
                if(v_et_had_jet[FPGA][TT]!=0): h_et_had_jet.Fill(v_et_had_jet[FPGA][TT]/1.e3)
                if(v_et_em_jet[FPGA][TT]!=0): h_et_em_jet.Fill(  v_et_em_jet[FPGA][TT]/1.e3)
                if(v_et_total_jet[FPGA][TT]!=0): h_et_total_jet.Fill(v_et_total_jet[FPGA][TT]/1.e3)
                if(v_et_had_met[FPGA][TT]!=0): h_et_had_met.Fill(v_et_had_met[FPGA][TT]/1.e3)
                if(v_et_em_met[FPGA][TT]!=0): h_et_em_met.Fill(v_et_em_met[FPGA][TT]/1.e3)
                if(v_et_total_met[FPGA][TT]!=0): h_et_total_met.Fill(v_et_total_met[FPGA][TT]/1.e3)
        

    print("Plotting..")
    h_rho_em    = SetHisto1D(h_rho_em,  " #rho_{EM} [GeV/(Area*#)]")
    h_rho_had1  = SetHisto1D(h_rho_had1," #rho_{HAD1} [GeV/(Area*#)]")
    h_rho_had2  = SetHisto1D(h_rho_had2," #rho_{HAD2} [GeV/(Area*#)]")
    h_rho_had3  = SetHisto1D(h_rho_had3," #rho_{HAD3} [GeV/(Area*#)]")
    h_rho_fcal  = SetHisto1D(h_rho_fcal," #rho_{FCAL} [GeV/(Area*#)]")
    h_et_had_jet  = SetHisto1D(h_et_had_jet," HAD layer Et [GeV] for Jets")
    h_et_em_jet  = SetHisto1D(h_et_em_jet," EM layer Et [GeV] for Jets")
    h_et_total_jet  = SetHisto1D(h_et_total_jet," Total Et [GeV] for Jets")
    h_et_had_met  = SetHisto1D(h_et_had_met," HAD layer Et [GeV] for Met")
    h_et_em_met  = SetHisto1D(h_et_em_met," EM layer Et [GeV] for Met")
    h_et_total_met  = SetHisto1D(h_et_total_met," Total Et [GeV] for Met")
    
    
    
    c1 = createCanvasPads("1")
    DrawCanvas(c1,h_rho_em)
    
    c2 = createCanvasPads("2")
    DrawCanvas(c2,h_rho_had1)
    
    c3 = createCanvasPads("3")
    DrawCanvas(c3,h_rho_had2)
    
    c4 = createCanvasPads("4")
    DrawCanvas(c4,h_rho_had3)
    
    c5 = createCanvasPads("5")
    DrawCanvas(c5,h_rho_fcal)
    
    c6 = createCanvasPads("6",1)
    DrawCanvas(c6,h_et_had_jet)
    
    c7 = createCanvasPads("7",1)
    DrawCanvas(c7,h_et_em_jet)
    
    c8 = createCanvasPads("8",1)
    DrawCanvas(c8,h_et_total_jet)
    
    c9 = createCanvasPads("9",1)
    DrawCanvas(c9,h_et_had_met)
    
    c10 = createCanvasPads("10",1)
    DrawCanvas(c10,h_et_em_met)
    
    c11 = createCanvasPads("11",1)
    DrawCanvas(c11,h_et_total_met)
    
    
    
    fol = _dir_+"/Pileup_plots/"
    c1.SaveAs(fol+"Rho_EM_layer.png")
    c2.SaveAs(fol+"Rho_HAD1_layer.png")
    c3.SaveAs(fol+"Rho_HAD2_layer.png")
    c4.SaveAs(fol+"Rho_HAD3_layer.png")
    c5.SaveAs(fol+"Rho_FCAL_layer.png")
    c6.SaveAs(fol+"Et_HAD_jets.png")
    c7.SaveAs(fol+"Et_EM_jets.png")
    c8.SaveAs(fol+"Et_Total_jets.png")
    c9.SaveAs(fol+"Et_HAD_met.png")
    c10.SaveAs(fol+"Et_EM_met.png")
    c11.SaveAs(fol+"Et_Total_met.png")
    
    outputFile  = ROOT.TFile.Open( fol+"Pileup_file.root" ,"RECREATE")
    h_rho_em.Write()
    h_rho_had1.Write()
    h_rho_had2.Write()
    h_rho_had3.Write()
    h_rho_fcal.Write()
    h_et_had_jet.Write()
    h_et_em_jet.Write()
    h_et_total_jet.Write()
    h_et_had_met.Write()
    h_et_em_met.Write()
    h_et_total_met.Write()
    outputFile.Close()
    
    
    
    print("Done..")


def createRatio(h1, h2, color, i):
    h3 = h1.Clone("h3")
    h3.GetXaxis().SetTitle("VBF BDT."+str(i))
    h3.SetLineColor(color)
    h3.SetMarkerStyle(20)
    h3.SetMarkerColor(color)
    h3.SetTitle("")
    #h3.SetMinimum(0.8)
    #h3.SetMaximum(1.35)
    # Set up plot for markers and errors
    h3.Sumw2()
    h3.SetStats(0)
    h3.Divide(h2)
    
    # Adjust y-axis settings
    y = h3.GetYaxis()
    y.SetTitle("train/test")
    y.SetNdivisions(505)
    y.SetTitleSize(20)
    y.SetTitleFont(43)
    y.SetTitleOffset(1.55)
    y.SetLabelFont(43)
    y.SetLabelSize(15)
    
    # Adjust x-axis settings
    x = h3.GetXaxis()
    x.SetTitleSize(20)
    x.SetTitleFont(43)
    x.SetTitleOffset(4.0)
    x.SetLabelFont(43)
    x.SetLabelSize(15)
    MyLowerLimit = 0.0
    MyUpperLimit = 2.0
    h3.GetYaxis().SetRangeUser(MyLowerLimit, MyUpperLimit)
    
    return h3


def createCanvasPads(name_,log = 0):

    c = TCanvas( name_, 'Dynamic Filling', 1500,1000 )
    c.SetTopMargin(0.07)     
    c.SetRightMargin(0.15)     
    c.SetLeftMargin(0.10)
    if(log): c.SetLogy()
    
    return c

def SetHisto(h,title):
  h.SetTitle(title)
  # h.GetXaxis().SetTitle("#eta [ "+str(round(h.GetXaxis().GetBinWidth(1),0))+" p/b ]")
  # h.GetYaxis().SetTitle("#phi [ "+str(round(h.GetYaxis().GetBinWidth(1),0))+" p/b ]")
  h.GetXaxis().SetTitle("#eta [ #times10 ]")
  h.GetXaxis().SetTitleOffset(1.0)
  h.GetYaxis().SetTitle("#phi [ #times10  ]")
  h.GetYaxis().SetTitleOffset(1.0)
  h.GetZaxis().SetTitle("Et (scale 200 MeV) [MeV]")
  #h_eta_v_phi_map.GetYaxis().SetNdivisions(0,1,0);
  h.SetLineColor(2)
  h.SetFillStyle(3325)
  h.SetFillColor(2)
  h.SetLineWidth(1)
  h.SetStats(0)
  return h
  
def SetHisto1D (h,title):
  h.SetTitle("")
  # h.GetXaxis().SetTitle("#eta [ "+str(round(h.GetXaxis().GetBinWidth(1),0))+" p/b ]")
  h.GetYaxis().SetTitle("Events / "+str(round(h.GetXaxis().GetBinWidth(1),3))+" GeV")
  h.GetXaxis().SetTitle(title)
  # h.GetYaxis().SetTitle("counts")
  #h_eta_v_phi_map.GetYaxis().SetNdivisions(0,1,0);
  h.SetLineColor(2)
  h.SetFillStyle(3002)
  h.SetFillColor(2)
  h.SetLineWidth(1)
  # h.SetStats(0)
  gStyle.SetOptStat(10)
  return h

def DrawCanvas(c,h,opt=""):
    # c.SetBatch(ROOT.kFALSE)
    c.cd()   
    h.Draw("COLZ"+opt)
    # gPad.RedrawAxis()
    c.Modified()
    c.Update()
      
def main(): 
    from optparse import OptionParser 
    parser = OptionParser(usage = "usage: %prog arguments", version="%prog") 
    parser.add_option('-d','--directory',action="store", type="string", dest='folder', help='Main directory of the validation  (default: %default)')
    parser.set_defaults(folder="Non")
    (options,args) = parser.parse_args()
    os.system("mkdir -p "+ options.folder+"/Pileup_plots")
    plotting("../myfile_jfex.root",options.folder)

if __name__ == '__main__':
    main()
