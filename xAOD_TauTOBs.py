#!/usr/bin/env python                                                                                                                                                                 
# -*- coding: utf-8 -*-  
import os    
import ROOT
ROOT.gROOT.SetBatch(True)
from ROOT import xAOD
from ROOT import TTree, TChain, TF1, TH1I, TH1F, TH1D, TH2F, TH2I, TH2D, TGraphErrors, TGraph, THStack, TLegend, TGraph2D, TLatex
from ROOT import TFile, TCanvas, TPad, TLine, TLorentzVector
from ROOT import gPad,gStyle
import array as arr


def ReadTOBs(_dir_):
    
    xAOD.Init()

    # Set up the input files
    fileName = '../xAOD.jFEX.output.root'
    treeName = 'CollectionTree'
    f = TFile.Open(fileName)

    # Make the "transient tree"
    event = ROOT.xAOD.TEvent (ROOT.xAOD.TEvent.kAthenaAccess)
    event.readFrom (f, True, 'CollectionTree')
    t = ROOT.xAOD.MakeTransientTree(event, 'CollectionTree')    
    # t = xAOD.MakeTransientTree(f, treeName)
    
    # Defining histos
    h_Et          = TH1F( 'et', 'et', 512, 0, 2048 )
    h_Eta         = TH1F( 'Local_eta', 'Local_eta', 16, 0, 16)
    h_Phi         = TH1F( 'Local_phi', 'Local_phi', 16, 0, 16 )    
    h_Eta_float   = TH1F( 'Float_eta', 'Float_eta', 100, -5.0, 5.0)
    x = []
    for i in range(67):
        phi = (-ROOT.TMath.Pi()- ROOT.TMath.Pi()/32) +ROOT.TMath.Pi()/32*i 
        x.append(phi)
    x = sorted(x)
    h_Phi_float   = TH1F( 'Float_phi', 'Float_phi', 66, arr.array('d',x)   )    
    h_local_phi_eta      = TH2I( '2Dphieta', '2Dphieta', 80, 0, 8, 160, 0, 16 )  
    h_phi_eta_global_int = TH2I( '2Dphieta_global_int', '2Dphieta_global_int', 100, -50, 50, 64, 0, 64 )  
    h_phi_eta_global_flo = TH2F( '2Dphieta_global_flo', '2Dphieta_global_flo', 100, -5.0, 5.0, 66,  arr.array('d',x)  )    


    # total_entries = t.GetEntries()
    total_entries = t.GetEntries()
    num_of_tobs = 0
    num_of_1_tobs = 0
    num_of_0_tobs = 0
    
    print('Number of input events: %s' % total_entries)
    # loop over collection
    for entry in range(total_entries):

        t.GetEntry(entry)

        for elem in t.L1_jFexTauRoI:
            
            num_of_tobs += 1
            if(elem.tobWord() == 0 ): 
                num_of_0_tobs += 1
                continue
            num_of_1_tobs += 1
            
            h_Et.Fill (elem.tobEt() ,1)
            h_Eta.Fill(elem.tobLocalEta()  ,1)
            h_Phi.Fill(elem.tobLocalPhi()  ,1)
            h_Eta_float.Fill(elem.eta()  ,1)
            h_Phi_float.Fill(elem.phi()  ,1)
            h_local_phi_eta.Fill(elem.tobLocalEta(),elem.tobLocalPhi()  ,1)
            h_phi_eta_global_int.Fill(elem.globalEta(),elem.globalPhi()  ,1)
            h_phi_eta_global_flo.Fill(elem.eta(),elem.phi()  ,1)
            
    print("-"*20)
    print("Total number of jTaus all : %8d (%d x 144 = %d)" %  (num_of_tobs,total_entries,total_entries*144))
    print("Total number of jTaus != 0: %8d (%5.1f %%)" %  (num_of_1_tobs,100*num_of_1_tobs/num_of_tobs))
    print("Total number of jTaus == 0: %8d (%5.1f %%)" %  (num_of_0_tobs,100*num_of_0_tobs/num_of_tobs))
    print("-"*20)
    
    h_Et = SetHisto(h_Et, "Et values of Taus", "tob Et [MeV/200] ")                                
    c1 = createCanvasPads("1")
    c1.SetLogy()
    DrawCanvas(c1,h_Et, "SAME")

    h_Eta = SetHisto(h_Eta, "tob #eta values of Taus", "tob #eta values")                                
    c2 = createCanvasPads("2")
    DrawCanvas(c2,h_Eta, "SAME")

    h_Phi = SetHisto(h_Phi, "tob #phi values of Taus", "tob #phi values")                                
    c3 = createCanvasPads("3")
    DrawCanvas(c3,h_Phi, "SAME")

    h_Eta_float = SetHisto(h_Eta_float, "#eta values of Taus", "#eta values")                                
    c4 = createCanvasPads("4")
    DrawCanvas(c4,h_Eta_float, "P SAME")

    h_Phi_float = SetHisto(h_Phi_float, "#phi values of Taus", "#phi values")                                
    c5 = createCanvasPads("5")
    # c5.SetLogy()
    DrawCanvas(c5,h_Phi_float, "P SAME")
    
    
    h_local_phi_eta.SetTitle("2D Local Phi-Eta Distribution - jTaus")
    h_local_phi_eta.GetXaxis().SetTitle("#eta values")
    h_local_phi_eta.GetYaxis().SetTitle("#phi values")   
    h_local_phi_eta.SetMarkerStyle(3)       
    h_local_phi_eta.GetYaxis().SetTitleOffset(1)           
    c6 = createCanvasPads("6")
    DrawCanvas(c6,h_local_phi_eta, "")


    h_phi_eta_global_int.SetTitle("2D Global Phi-Eta Distribution - jTaus")
    h_phi_eta_global_int.GetXaxis().SetTitle("#eta values")
    h_phi_eta_global_int.GetYaxis().SetTitle("#phi values")   
    h_phi_eta_global_int.SetMarkerStyle(3)      
    h_phi_eta_global_int.GetYaxis().SetTitleOffset(1)            
    c7 = createCanvasPads("7")
    DrawCanvas(c7,h_phi_eta_global_int, "COLZ")

    h_phi_eta_global_flo.SetTitle("2D Phi-Eta Distribution - jTaus")
    h_phi_eta_global_flo.GetXaxis().SetTitle("#eta values")
    h_phi_eta_global_flo.GetYaxis().SetTitle("#phi values") 
    h_phi_eta_global_flo.GetYaxis().SetTitleOffset(1)     
    h_phi_eta_global_flo.SetMarkerStyle(3)                  
    c8 = createCanvasPads("8")
    DrawCanvas(c8,h_phi_eta_global_flo, "")
    
    
    c1.SaveAs(_dir_+"/Tau_plots/TotalEt.png")
    c2.SaveAs(_dir_+"/Tau_plots/LocalEta.png")
    c3.SaveAs(_dir_+"/Tau_plots/LocalPhi.png")
    c4.SaveAs(_dir_+"/Tau_plots/Eta.png")
    c5.SaveAs(_dir_+"/Tau_plots/Phi.png")
    c6.SaveAs(_dir_+"/Tau_plots/2D_LocalEtaPhi.png")
    c7.SaveAs(_dir_+"/Tau_plots/2D_IntEtaPhi.png")
    c8.SaveAs(_dir_+"/Tau_plots/2D_FloatEtaPhi.png")
    
    outfile = TFile( _dir_+"/Tau_plots/Tau_file.root", 'RECREATE' )
    h_Et.Write()
    h_Eta.Write()
    h_Phi.Write()
    h_Eta_float.Write()
    h_Phi_float.Write()
    h_local_phi_eta.Write()
    h_phi_eta_global_int.Write()
    h_phi_eta_global_flo.Write()
    outfile.Close()    
    
    # input()
    print("Done..")


def createCanvasPads(name_):
    c = TCanvas( name_, 'Dynamic Filling', 1500,1000 )
    c.SetTopMargin(0.07)
    c.SetRightMargin(0.10)
    c.SetLeftMargin(0.10)

    return c

def SetHisto(h,title, xTitle=""):
    h.SetTitle(title)
    h.GetXaxis().SetTitle(xTitle)
    h.GetYaxis().SetTitle("Number of TOBs")
    h.GetYaxis().SetTitleOffset(1)
    # h.GetZaxis().SetTitle("Et (scale 200 MeV) [MeV]")
    # h_eta_v_phi_map.GetYaxis().SetNdivisions(0,1,0);
    h.SetLineColor(2)
    h.SetFillStyle(3002)
    h.SetFillColor(2)
    h.SetLineWidth(1)
    h.SetStats(0)
    # gStyle.SetOptStat(10)
    return h

def DrawCanvas(c,h,opt=""):
    c.SetBatch(ROOT.kFALSE)
    c.cd()   
    # h.Draw("COLZ"+opt)
    h.Draw(opt)
    gPad.RedrawAxis()
    c.Modified()
    c.Update()

def main(): 
    
    from optparse import OptionParser 
    parser = OptionParser(usage = "usage: %prog arguments", version="%prog") 
    parser.add_option('-d','--directory',action="store", type="string", dest='folder', help='Main directory of the validation  (default: %default)')
    parser.set_defaults(folder="Non")
    (options,args) = parser.parse_args()
    os.system("mkdir -p "+ options.folder+"/Tau_plots")
    ReadTOBs(options.folder)

if __name__ == '__main__':   
    main()
