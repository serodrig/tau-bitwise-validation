#!/usr/bin/env python                                                                                                                                                                 
# -*- coding: utf-8 -*-  
import os       
import ROOT
ROOT.gROOT.SetBatch(True)
from ROOT import xAOD
from ROOT import TTree, TChain, TF1, TH1I, TH1F, TH1D, TH2F, TH2I, TH2D, TGraphErrors, TGraph, THStack, TLegend, TGraph2D, TLatex, TMath
from ROOT import TFile, TCanvas, TPad, TLine, TLorentzVector
from ROOT import gPad,gStyle
import array as arr


def ReadTOBs(_dir_):
    
    xAOD.Init()

    # Set up the input files
    fileName = '../xAOD.jFEX.output.root'
    treeName = 'CollectionTree'
    f = TFile.Open(fileName)

    # Make the "transient tree"
    event = ROOT.xAOD.TEvent (ROOT.xAOD.TEvent.kAthenaAccess)
    event.readFrom (f, True, 'CollectionTree')
    t = ROOT.xAOD.MakeTransientTree(event, 'CollectionTree')    
    # t = xAOD.MakeTransientTree(f, treeName)
    
    # Print some information
    print('Number of input events: %s' % t.GetEntries())


    # Defining histos
    h_tobEx       = TH1F( 'tobEx', 'tobEx', 350, -3.5, 3.5 )
    h_tobEy       = TH1F( 'tobEy', 'tobEy', 350, -3.5, 3.5 )
    h_Ex          = TH1F( 'Ex', 'Ex', 350, -3.5, 3.5 )
    h_Ey          = TH1F( 'Ey', 'Ey', 350, -3.5, 3.5 )
    h_met         = TH1F( 'met', 'met', 200, 0, 4 )
    h_phi         = TH1F( 'phi', 'phi', 64, -3.2, 3.2 )

    # loop over electron collection
    for entry in range(t.GetEntries()):
        t.GetEntry(entry)
        
        ex = 0
        ey = 0
        for elem in t.L1_jFexMETRoI:
            
            s="Module: %1d   FPGA: %1d - Ex: %6d Ey: %6d   ---> TOB: %15d" % (elem.jFexNumber(),elem.fpgaNumber(),elem.tobEx(),elem.tobEy(),elem.tobWord())
            # print(s)

            h_tobEx.Fill(elem.tobEx()/1e3 ,1)
            h_tobEy.Fill(elem.tobEy()/1e3 ,1)
            ex += elem.tobEx()
            ey += elem.tobEy()
            
        h_phi.Fill(TMath.ATan2(ey,ex)  ,1)
        h_Ex.Fill(ex/1e3  ,1)
        h_Ey.Fill(ey/1e3 ,1)
        h_met.Fill(TMath.Sqrt(pow(ex,2)+pow(ey,2))/1e3  ,1)

    h_tobEx = SetHisto(h_tobEx, "Ex values of jMET", "tob Ex [GeV/200] ")                                
    c1 = createCanvasPads("1")
    DrawCanvas(c1,h_tobEx, "SAME")

    h_tobEy = SetHisto(h_tobEy, "Ey values of jMET", "tob Ey [GeV/200] ")                                
    c2 = createCanvasPads("2")
    DrawCanvas(c2,h_tobEy, "SAME")

    h_phi = SetHisto(h_phi, "Atan(#phi) values of jMET", "#phi=atan(Ey/Ex) values")                                
    c3 = createCanvasPads("3")
    DrawCanvas(c3,h_phi, "SAME")

    h_Ex = SetHisto(h_Ex, "Summed Ex values of jMET", "Summed Ex [GeV/200] ")                                
    c4 = createCanvasPads("4")
    DrawCanvas(c4,h_Ex, "SAME")

    h_Ey = SetHisto(h_Ey, "Summed Ey values of jMET", "Summed Ey [GeV/200] ")                                
    c5 = createCanvasPads("5")
    DrawCanvas(c5,h_Ey, "SAME")

    h_met = SetHisto(h_met, "jMET Et [GeV/200]", "jXE Et [GeV/200] ")
    c6 = createCanvasPads("6")
    c6.SetLogy() 
    DrawCanvas(c6,h_met, "SAME")

    c1.SaveAs(_dir_+"/MET_plots/tobEx.png")
    c2.SaveAs(_dir_+"/MET_plots/tobEy.png")
    c3.SaveAs(_dir_+"/MET_plots/Phi.png")
    c4.SaveAs(_dir_+"/MET_plots/Ex.png")
    c5.SaveAs(_dir_+"/MET_plots/Ey.png")
    c6.SaveAs(_dir_+"/MET_plots/met.png")
    
    outfile = TFile( _dir_+"/MET_plots/MET_file.root", 'RECREATE' )
    h_tobEx.Write()
    h_tobEy.Write()
    h_phi.Write()
    h_Ex.Write()
    h_Ey.Write()
    h_met.Write()
    outfile.Close()    
    # input()
    print("Done..")


def convert(val):
    sign = (val >> 14) & 0x1
    print("sign: ",sign)
    
    
    
def createCanvasPads(name_):
    c = TCanvas( name_, 'Dynamic Filling', 1500,1000 )
    c.SetTopMargin(0.07)
    c.SetRightMargin(0.10)
    c.SetLeftMargin(0.10)

    return c

def SetHisto(h,title, xTitle="", show = False):
    h.SetTitle(title)
    h.GetXaxis().SetTitle(xTitle)
    h.GetYaxis().SetTitle("Number of TOBs")
    h.GetYaxis().SetTitleOffset(1)
    # h.GetZaxis().SetTitle("Et (scale 200 MeV) [MeV]")
    # h_eta_v_phi_map.GetYaxis().SetNdivisions(0,1,0);
    h.SetLineColor(2)
    h.SetFillStyle(3002)
    h.SetFillColor(2)
    h.SetLineWidth(1)
    if(show):
        gStyle.SetOptStat(10)
    else:
        h.SetStats(0)
    
    return h

def DrawCanvas(c,h,opt=""):
    c.SetBatch(ROOT.kFALSE)
    c.cd()   
    # h.Draw("COLZ"+opt)
    h.Draw(opt)
    gPad.RedrawAxis()
    c.Modified()
    c.Update()

def main(): 
    
    from optparse import OptionParser 
    parser = OptionParser(usage = "usage: %prog arguments", version="%prog") 
    parser.add_option('-d','--directory',action="store", type="string", dest='folder', help='Main directory of the validation  (default: %default)')
    parser.set_defaults(folder="Non")
    (options,args) = parser.parse_args()
    os.system("mkdir -p "+ options.folder+"/MET_plots")
    ReadTOBs(options.folder)

if __name__ == '__main__':   
    main()
