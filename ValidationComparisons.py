#!/usr/bin/env python3                                                                                                                                                            
# -*- coding: utf-8 -*-                                                                                                                                                               
import os,sys
import ROOT
ROOT.gROOT.SetBatch(True)
from ROOT import TTree, TChain, TF1, TH1I, TH1F, TH1D, TH2F, TH2I, TH2D, TGraphErrors, TGraph, THStack, TLegend, TGraph2D, TLatex
from ROOT import TFile, TCanvas, TPad, TLine, TLorentzVector, TIter
from ROOT import gPad,gStyle,gDirectory

RED      ="\033[1;31m";
ORANGE   ="\033[1;38;5;208m";
YELLOW   ="\033[1;33m";
GREEN    ="\033[1;32m";
BLUE     ="\033[1;34m";
END      ="\033[0m";



def Comparison(outfile,old_path,new_path,_file_,_N_):

    fileName_old = old_path+"/"+_file_
    print(BLUE+"Old file to compare with:  "+fileName_old+END)
    f_old = TFile.Open(fileName_old)
    f_old.ls()
    
    fileName_new = new_path+"/"+_file_
    print(BLUE+"New file:  "+fileName_new+END)
    f_new = TFile.Open(fileName_new)
    f_new.ls()    
    
    
    f_new.cd()
    for key in gDirectory.GetListOfKeys():
        obj = key.ReadObj()
        # print(key,obj.IsA().GetName())
        
        if(obj.IsA().GetName() == "TH1F"):
            h_old = f_old.Get(key.GetName())
            h_old = SetHisto(h_old,4,3004)
            h_new = f_new.Get(key.GetName())
            h_new = SetHisto(h_new,2,3005)
            
            
            legend = ROOT.TLegend (0.7 ,0.75 ,0.95 ,0.9)
            legend.SetHeader(_N_,"C");
            legend.AddEntry(h_old ,"r"+old_path)
            legend.AddEntry(h_new ,"r"+new_path)
            legend.SetLineWidth(0)
            
            
            hratio = createRatio(h_old, h_new)
            c, pad1, pad2 = createCanvasPads(key.GetName())
            pad1.cd()
            h_old.Draw()
            h_new.Draw("same")      
            legend.Draw("same")
            
            pad2.cd()
            hratio.Draw("P")  
            _min_ = hratio.GetXaxis().GetXmin()
            _max_ = hratio.GetXaxis().GetXmax()
            l = TLine(_min_,1,_max_,1)
            l.SetLineColor(2)
            l.Draw("same")
            c.Modified()
            c.Update()  
            c.SetName(_N_+"_"+key.GetName())
            outfile.cd()
            c.Write()
            c.SaveAs(new_path+"/Comparisons_wrt_"+old_path+"/"+_N_+"_"+key.GetName()+".png")
            


    print("Done..")


def SetHisto(h,color,fill):
    h.SetLineColor(color)
    h.SetFillStyle(fill)
    h.SetFillColor(color)
    h.SetLineWidth(1)
    h.SetStats(0)
    return h


def createRatio(h1, h2):
    h3 = h1.Clone("h3")
    h3.SetLineColor(1)
    h3.SetTitle("")
    h3.SetMinimum(0.0)
    h3.SetMaximum(2.0)
    # Set up plot for markers and errors
    h3.SetStats(0)
    h3.Divide(h2)
 
    # Adjust y-axis settings
    y = h3.GetYaxis()
    y.SetTitle("ratio old/new ")
    y.SetNdivisions(505)
    y.SetTitleSize(20)
    y.SetTitleFont(43)
    y.SetTitleOffset(1.55)
    y.SetLabelFont(43)
    y.SetLabelSize(15)
 
    # Adjust x-axis settings
    x = h3.GetXaxis()
    x.SetTitleSize(20)
    x.SetTitleFont(43)
    x.SetTitleOffset(4.0)
    x.SetLabelFont(43)
    x.SetLabelSize(15)
 
    return h3
 
 
def createCanvasPads(name):
    c = TCanvas(name, "canvas", 800, 800)
    # Upper histogram plot is pad1
    pad1 = TPad("pad1", "pad1", 0, 0.3, 1, 1.0)
    pad1.SetBottomMargin(0)  # joins upper and lower plot
    pad1.SetGridx()
    pad1.SetLogy()
    pad1.Draw()
    # Lower ratio plot is pad2
    c.cd()  # returns to main canvas before defining pad2
    pad2 = TPad("pad2", "pad2", 0, 0.05, 1, 0.3)
    pad2.SetTopMargin(0)  # joins upper and lower plot
    pad2.SetBottomMargin(0.2)
    pad2.SetGridx()
    pad2.Draw()
 
    return c, pad1, pad2

def main(): 
    
    from optparse import OptionParser 
    parser = OptionParser(usage = "usage: %prog arguments", version="%prog") 
    parser.add_option('--d_old','--directory1',action="store", type="string", dest='folder_old', help='Main directory of the validation  (default: %default)')
    parser.add_option('--d_new','--directory2',action="store", type="string", dest='folder_new', help='Main directory of the validation  (default: %default)')
    parser.set_defaults(folder_old="Non_dold")
    parser.set_defaults(folder_new="Non_dnew")
    (options,args) = parser.parse_args()

    os.system("mkdir -p "+ options.folder_new+"/Comparisons_wrt_"+options.folder_old)
    outfile = TFile( options.folder_new+"/Comparisons_wrt_"+options.folder_old+"/Compared_plots_with_r"+options.folder_old+".root", 'RECREATE' )
    
    Comparison(outfile,options.folder_old,options.folder_new,"Tau_plots/Tau_file.root","jtau")
    Comparison(outfile,options.folder_old,options.folder_new,"SRJets_plots/SRJet_file.root","jJ")
    Comparison(outfile,options.folder_old,options.folder_new,"LRJets_plots/LRJet_file.root","jLJ")
    Comparison(outfile,options.folder_old,options.folder_new,"MET_plots/MET_file.root","jXE")
    Comparison(outfile,options.folder_old,options.folder_new,"SumET_plots/SumET_file.root","jTE")
    Comparison(outfile,options.folder_old,options.folder_new,"Pileup_plots/Pileup_file.root","jPileup")
    
    outfile.Close()

if __name__ == '__main__':   
    main()
