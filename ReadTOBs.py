#!/usr/bin/env python                                                                                                                                                                 
# -*- coding: utf-8 -*-                                                                                                                                                               
import os,sys
import ROOT
from array import array
import inspect
import random                                                                                                                                   
from ROOT import gROOT

from ROOT import TTree, TChain, TF1, TH1I, TH1F, TH1D, TH2F, TH2D, TGraphErrors, TGraph, THStack, TLegend, TGraph2D, TLatex
from ROOT import TFile, TCanvas, TPad, TLine, TLorentzVector
from ROOT import gPad,gStyle

folder ="../"

def main(): 
    ReadTOBs("myfile_jfex.root")
    input()

def ReadTOBs(_file_):
    

    print("Opening file..")
    print(folder + _file_)
    rootFile = TFile(folder + _file_)
    print(rootFile.ls())
    tree = rootFile.Get("data")
    print( tree)
    v_tau_TOB_word = ROOT.vector('uint32_t')()
    v_tau_isMax    = ROOT.vector('int')()
    v_tau_jFEXid   = ROOT.vector('int')()
    v_tau_FPGAid   = ROOT.vector('int')()

    print("Setting up branches..")
    tree.SetBranchStatus('*',0)
    tree.SetBranchStatus('tau*',1)
    tree.SetBranchAddress("tau_TOB_word"  , v_tau_TOB_word)
    tree.SetBranchAddress("tau_isLocalMax", v_tau_isMax)    
    tree.SetBranchAddress("tau_jFEXid"    , v_tau_jFEXid)
    tree.SetBranchAddress("tau_FPGAid"    , v_tau_FPGAid)
    
                    
    # Defining histos
    h_Et          = TH1F( 'et', 'et', 100, 0, 200 )
    h_Eta         = TH1F( 'eta', 'eta', 8, 0, 8)
    h_Eta_global  = TH1F( 'eta_g', 'eta', 30, -15, 15)
    h_Phi         = TH1F( 'phi', 'phi', 16, 0, 16 )    
    h_Phi_global  = TH1F( 'phi_g', 'phi', 65, 0, 65 )    

    print("Looping over entries..",tree.GetEntries())
    for entry in range(tree.GetEntries()):
        tree.GetEntry(entry)
        jfex_fpga_tobs = {}
        for j in range(v_tau_TOB_word.size()):
            if(v_tau_isMax[j]!=1): continue
            m_tob_word = format(v_tau_TOB_word[j],"032b")
            fpga_id = v_tau_FPGAid[j]
            jfex_id = v_tau_jFEXid[j]
            m_eta=m_tob_word[:5]
            m_phi=m_tob_word[5:9]
            m_et =m_tob_word[9:20]
            m_iso=m_tob_word[20:31]
            m_sat=m_tob_word[31:]
            
            m_global_eta =  int(m_eta,2) + (8*(jfex_id -3) +0.5);
            m_global_phi =  int(m_phi,2) + (fpga_id * 16);
            
            if not jfex_id in jfex_fpga_tobs:
                jfex_fpga_tobs[jfex_id] = {}
            fpga_tobs = jfex_fpga_tobs[jfex_id]

            if not fpga_id in fpga_tobs:
                fpga_tobs[fpga_id] = []                
            tobs = fpga_tobs[fpga_id]            
            
            tob_entry = (int(m_et, 2), int(m_phi,2), int(m_eta,2), int(m_iso,2), int(m_sat,2), m_tob_word, m_global_eta, m_global_phi)
            tobs.append(tob_entry)
            
        for jfex_id, fpga_tobs in jfex_fpga_tobs.items():
            for fpga_id, tobs in fpga_tobs.items():
                # print(f"{jfex_id} {fpga_id}: {len(tobs)} entries")
                while len(tobs) <= 6:
                    tobs.append((0, 0, 0, 0, format(0,"032b"), 0, 0))

                # Sort by et (first entry):
                tobs_sorted = sorted(tobs, reverse=True, key=lambda entry: entry[0])

                # Limit to 6
                tobs_sorted = tobs_sorted[:6]
                for i, (et, phi, eta, iso, sat, word, global_eta, global_phi) in enumerate(tobs_sorted):
                    print(int(word,2),  eta, phi, et, iso, sat,  global_eta, global_phi)
                    if(word != format(0,"032b")): 
                        h_Eta.Fill(eta)
                        h_Phi.Fill(phi)
                        h_Et.Fill(et)
                        h_Eta_global.Fill(global_eta)
                        h_Phi_global.Fill(global_phi)
                        
                
                
    h_Eta = SetHisto(h_Eta, "Local FPGA #eta values of Taus", "#eta values")
    h_Phi = SetHisto(h_Phi, "Local FPGA #phi values of Taus", "#phi values")
    h_Et  = SetHisto(h_Et, "Et values of Taus", "E_{T} values")
    h_Eta_global = SetHisto(h_Eta_global, "Global #eta values of Taus", "#eta values")
    h_Phi_global = SetHisto(h_Phi_global, "Global #phi values of Taus", "#phi values")                                

    c1 = createCanvasPads("1")
    DrawCanvas(c1,h_Eta, "SAME")
    # c1.SaveAs("SR_localfpga_eta_no_zeros.png")

    c2 = createCanvasPads("2")
    DrawCanvas(c2, h_Phi)
    # c2.SaveAs("SR_localfpga_phi_no_zeros.png")

    c3 = createCanvasPads("3")
    DrawCanvas(c3, h_Et)
    # c3.SaveAs("SR_TOB_Energy_no_zeros.png")
    
    c4 = createCanvasPads("4")
    DrawCanvas(c4, h_Eta_global)
    # c4.SaveAs("SR_globaleta_no_zeros.png")
    
    c5 = createCanvasPads("5")
    DrawCanvas(c5, h_Phi_global)
    # c5.SaveAs("SR_globalphi_no_zeros.png")

  


    print("Done..")

def createCanvasPads(name_):
    c = TCanvas( name_, 'Dynamic Filling', 1500,1000 )
    c.SetTopMargin(0.07)
    c.SetRightMargin(0.15)
    c.SetLeftMargin(0.10)

    return c

def SetHisto(h,title, xTitle=""):
  h.SetTitle(title)
  h.GetXaxis().SetTitle(xTitle)
  h.GetYaxis().SetTitle("Number of TOBs")
#  h.GetZaxis().SetTitle("Et (scale 200 MeV) [MeV]")
  #h_eta_v_phi_map.GetYaxis().SetNdivisions(0,1,0);
  h.SetLineColor(2)
  h.SetFillStyle(3325)
  h.SetFillColor(2)
  h.SetLineWidth(1)
  # h.SetStats(01)
  gStyle.SetOptStat(10);
  return h

def DrawCanvas(c,h,opt=""):
    c.SetBatch(ROOT.kFALSE)
    c.cd()   
    h.Draw("COLZ"+opt)
    gPad.RedrawAxis()
    c.Modified()
    c.Update()

if __name__ == '__main__':   
    main()
