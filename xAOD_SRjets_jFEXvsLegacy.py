#!/usr/bin/env python                                                                                                                                                                 
# -*- coding: utf-8 -*-         
import ROOT
from ROOT import xAOD
from ROOT import TTree, TChain, TF1, TH1I, TH1F, TH1D, TH2F, TH2I, TH2D, TGraphErrors, TGraph, THStack, TLegend, TGraph2D, TLatex
from ROOT import TFile, TCanvas, TPad, TLine, TLorentzVector, TMath
from ROOT import gPad,gStyle
import math

def ReadTOBs(_file_):
    
    xAOD.Init()

    # Set up the input files
    # fileName = '../Varsiha.xAOD.jFEX.output.root'
    fileName = '../4000.xAOD.jFEX.output.root'
    treeName = 'CollectionTree'
    f = TFile.Open(fileName)
    
    short = 0
    Nshort = 3

    # Make the "transient tree"
    t = xAOD.MakeTransientTree(f, treeName)
    
    # Print some information
    print('Number of input events: %s' % t.GetEntries())

    # Defining histos  
    h_tobphi      = TH1I('toblocalphi', 'toblocalphi', 16, 0, 16)
    h_tobeta      = TH1I('toblocaleta', 'toblocaleta', 32, 0, 32)
    h_tobEt       = TH1F('tobet','tobet', 2048, 0, 2048)
    h_tobSat      = TH1F('tobsat', 'tobsat', 2, 0,2)

    h_phi         = TH1F( 'phi', 'phi', 64, 0, 64 )
    h_eta         = TH1F( 'eta', 'eta', 100, -50, 50)
    h_Et          = TH1I( 'et', 'et', 200, 0, 10000 )

    h_phi_float   = TH1F( 'phif', 'phif', 65, 0, 6.5 )
    h_eta_float   = TH1F( 'etaf', 'etaf', 100, -5.0, 5.0)



    h_phi_eta_glo_int     = TH2I( '2Dphieta_glo_int', '2Dphieta_glo_int', 10000, -50, 50, 10000, 0, 64 )    
    h_phi_eta_glo_float   = TH2F( '2Dphieta_glo_float', '2Dphieta_glo_float', 1000, -5, 5,1000, 0, 7 )    
    h_phi_eta_local       = TH2I( '2Dphieta_local', '2Dphieta_local', 100, 0, 25, 100, 0, 16 )


    h_phi_eta_intimeantikt       = TH2F( 'antikt', 'antikt', 1000, -5, 5,1000, 0, 7 )    
    h_phi_eta_LVL1JetRoIs        = TH2F( 'LVL1JetRoIs', 'LVL1JetRoIs', 1000, -5, 5,1000, 0, 7 )    
    h_phi_eta_AntiKt4EMPFlowJets = TH2F( 'AntiKt4EMPFlowJets', 'AntiKt4EMPFlowJets', 1000, -5, 5,1000, 0, 7 )    
    h_phi_eta_AntiKt4EMTopoJets  = TH2F( 'AntiKt4EMTopoJets', 'AntiKt4EMTopoJets', 1000, -5, 5,1000, 0, 7 )    
    h_phi_eta_AntiKt4LCTopoJets  = TH2F( 'AntiKt4LCTopoJets', 'AntiKt4LCTopoJets', 1000, -5, 5,1000, 0, 7 )
    
        
    h_eta_eta_Run3_Run2 = TH2F( 'h_eta_eta_Run3_Run2', 'h_eta_eta_Run3_Run2', 1000, -5, 5, 1000, -5, 5 )    
    h_phi_phi_Run3_Run2 = TH2F( 'h_phi_phi_Run3_Run2', 'h_phi_phi_Run3_Run2',1000, 0, 7,1000, 0, 7 )
        
    h_eta_eta_Run3_PFlow = TH2F( 'h_eta_eta_Run3_PFlow', 'h_eta_eta_Run3_PFlow', 1000, -5, 5, 1000, -5, 5 )    
    h_phi_phi_Run3_PFlow = TH2F( 'h_phi_phi_Run3_PFlow', 'h_phi_phi_Run3_PFlow',1000, 0, 7,1000, 0, 7 )
        
    h_eta_eta_Run3_EMTopo = TH2F( 'h_eta_eta_Run3_EMTopo', 'h_eta_eta_Run3_EMTopo', 1000, -5, 5, 1000, -5, 5 )    
    h_phi_phi_Run3_EMTopo = TH2F( 'h_phi_phi_Run3_EMTopo', 'h_phi_phi_Run3_EMTopo',1000, 0, 7,1000, 0, 7 )
        
    h_eta_eta_Run3_LCTopo = TH2F( 'h_eta_eta_Run3_LCTopo', 'h_eta_eta_Run3_LCTopo', 1000, -5, 5, 1000, -5, 5 )    
    h_phi_phi_Run3_LCTopo = TH2F( 'h_phi_phi_Run3_LCTopo', 'h_phi_phi_Run3_LCTopo',1000, 0, 7,1000, 0, 7 )    
    
    Nentry = 0
    Njets_run2 = 0
    Njets_matched_run3 = 0
    for entry in range(t.GetEntries()):
        t.GetEntry(entry)
        # t.GetEntry(49)
        print("*"*50,Nentry,"*"*50)
        Nentry+=1
        print("L1_jFexSRJetRoI: ")
        jets_run3 = {}
        cont = 0
        for elem in t.L1_jFexSRJetRoI:
            
            
            if(elem.tobWord()==0): continue
            cont+=1
            h_tobphi.Fill(elem.tobLocalPhi())
            h_tobeta.Fill(elem.tobLocalEta())
            h_tobEt.Fill(elem.tobEt())
            h_tobSat.Fill(elem.tobSat())
            h_phi.Fill(elem.globalPhi())
            h_eta.Fill(elem.globalEta())
            h_Et.Fill(elem.et()/1000)
            h_phi_float.Fill(elem.phi())
            h_eta_float.Fill(elem.eta())
            h_phi_eta_glo_int.Fill(elem.globalEta(), elem.globalPhi())
            # h_phi_eta_glo_float.Fill(elem.eta(), elem.phi())
            h_phi_eta_local.Fill(elem.tobLocalEta(), elem.tobLocalPhi())
            name = "---------> Central " + str(elem.jFexNumber())
            if(elem.jFexNumber() == 0 or elem.jFexNumber() == 5): name = "---------> Forward " + str(elem.jFexNumber())
            jets_run3[cont]={"eta":elem.eta(),"phi":elem.phi(),"et":elem.et(),"jfex":name}
            # print (format(elem.eta(),"5.2f"), format(elem.phi(),"5.2f"),format("","5s"),format(elem.et(),"10.2f"))
        
        jets_sorted_run3 = sorted(jets_run3, reverse=True, key=lambda entry: jets_run3[entry]["et"])
        if(short): jets_sorted_run3 = jets_sorted_run3[:Nshort]
        for Njet in jets_sorted_run3:
            # h_phi_eta_glo_float.Fill(jets_run3[Njet]["eta"],jets_run3[Njet]["phi"])
            print(format(Njet,"3d"),format(jets_run3[Njet]["eta"],"5.2f"),format(jets_run3[Njet]["phi"],"5.2f"),format(jets_run3[Njet]["et"],"10.2f"),jets_run3[Njet]["jfex"])
        
        # print("InTimeAntiKt4TruthJets: ")
        jets = {}
        cont = 0        
        for elem in t.InTimeAntiKt4TruthJets:
        # for elem in t.OutOfTimeAntiKt4TruthJets:
            phi = elem.phi()
            if(elem.phi() < 0): phi = elem.phi()+2*TMath.Pi()
            
            # h_phi_eta_intimeantikt.Fill(elem.eta(), phi)
            cont+=1
            jets[cont]={"eta":elem.eta(),"phi":phi,"et":elem.pt()}
            # print (format(elem.eta(),"5.2f"), format(elem.phi(),"5.2f"),format(phi,"5.2f"),format(elem.pt(),"10.2f"))
            
        jets_sorted = sorted(jets, reverse=True, key=lambda entry: jets[entry]["et"])
        if(short): jets_sorted = jets_sorted[:Nshort]    
        for Njet in jets_sorted:  
            # print(format(Njet,"3d"),format(jets[Njet]["eta"],"5.2f"),format(jets[Njet]["phi"],"5.2f"),format(jets[Njet]["et"],"10.2f"))
            h_phi_eta_intimeantikt.Fill(jets[Njet]["eta"],jets[Njet]["phi"])
            
             
        print("LVL1JetRoIs: ")  
        jets_run2 = {}
        cont = 0                
        for elem in t.LVL1JetRoIs:
            phi = elem.phi()
            if(elem.phi() < 0): phi = elem.phi()+2*TMath.Pi()
            
            # h_phi_eta_LVL1JetRoIs.Fill(elem.eta(), phi)
            cont+=1
            jets_run2[cont]={"eta":elem.eta(),"phi":phi,"et":elem.et4x4()}            
            # print (format(elem.eta(),"5.2f"), format(elem.phi(),"5.2f"),format(phi,"5.2f"),format(elem.et4x4(),"10.2f"))
            
        jets_sorted_run2 = sorted(jets_run2, reverse=True, key=lambda entry: jets_run2[entry]["et"])
        if(short): jets_sorted_run2 = jets_sorted_run2[:Nshort]    
        for Njet in jets_sorted_run2:  
            print(format(Njet,"3d"),format(jets_run2[Njet]["eta"],"5.2f"),format(jets_run2[Njet]["phi"],"5.2f"),format(jets_run2[Njet]["et"],"10.2f"))
            # h_phi_eta_LVL1JetRoIs.Fill(jets_run2[Njet]["eta"],jets_run2[Njet]["phi"])            
            
            
            
        # print("AntiKt4EMPFlowJets: ")  
        jets = {}
        cont = 0           
        for elem in t.AntiKt4EMPFlowJets:
            phi = elem.phi()
            if(elem.phi() < 0): phi = elem.phi()+2*TMath.Pi()
            
            # h_phi_eta_AntiKt4EMPFlowJets.Fill(elem.eta(), phi)
            cont+=1
            jets[cont]={"eta":elem.eta(),"phi":phi,"et":elem.e()}              
            # print (format(elem.eta(),"5.2f"), format(elem.phi(),"5.2f"),format(phi,"5.2f"),format(elem.pt(),"10.2f"))

        jets_sorted_PFlow = sorted(jets, reverse=True, key=lambda entry: jets[entry]["et"])
        if(short): jets_sorted_PFlow = jets_sorted_PFlow[:Nshort]    
        for Njet in jets_sorted_PFlow:  
            # print(format(Njet,"3d"),format(jets[Njet]["eta"],"5.2f"),format(jets[Njet]["phi"],"5.2f"),format(jets[Njet]["et"],"10.2f"))
            h_phi_eta_AntiKt4EMPFlowJets.Fill(jets[Njet]["eta"],jets[Njet]["phi"])             
            
            
            
            
        # print("AntiKt4EMTopoJets: ") 
        jets = {}
        cont = 0            
        for elem in t.AntiKt4EMTopoJets:
            phi = elem.phi()
            if(elem.phi() < 0): phi = elem.phi()+2*TMath.Pi()
            
            # h_phi_eta_AntiKt4EMTopoJets.Fill(elem.eta(), phi)
            cont+=1
            jets[cont]={"eta":elem.eta(),"phi":phi,"et":elem.e()}  
            # print (format(elem.eta(),"5.2f"), format(elem.phi(),"5.2f"),format(phi,"5.2f"),format(elem.pt(),"10.2f"))

        jets_sorted_EMTopo = sorted(jets, reverse=True, key=lambda entry: jets[entry]["et"])
        if(short): jets_sorted_EMTopo = jets_sorted_EMTopo[:Nshort]    
        for Njet in jets_sorted_EMTopo: 
            # print(format(Njet,"3d"),format(jets[Njet]["eta"],"5.2f"),format(jets[Njet]["phi"],"5.2f"),format(jets[Njet]["et"],"10.2f")) 
            h_phi_eta_AntiKt4EMTopoJets.Fill(jets[Njet]["eta"],jets[Njet]["phi"]) 
            
            
            
            
        # print("AntiKt4LCTopoJets: ")  
        jets = {}
        cont = 0         
        for elem in t.AntiKt4LCTopoJets:
            phi = elem.phi()
            if(elem.phi() < 0): phi = elem.phi()+2*TMath.Pi()
            
            # h_phi_eta_AntiKt4LCTopoJets.Fill(elem.eta(), phi)
            cont+=1
            jets[cont]={"eta":elem.eta(),"phi":phi,"et":elem.e()}  
            # print (format(elem.eta(),"5.2f"), format(elem.phi(),"5.2f"),format(phi,"5.2f"),format(elem.pt(),"10.2f"))

        jets_sorted_LCTopo = sorted(jets, reverse=True, key=lambda entry: jets[entry]["et"])
        if(short): jets_sorted_LCTopo = jets_sorted_LCTopo[:Nshort]    
        for Njet in jets_sorted_LCTopo: 
            # print(format(Njet,"3d"),format(jets[Njet]["eta"],"5.2f"),format(jets[Njet]["phi"],"5.2f"),format(jets[Njet]["et"],"10.2f")) 
            h_phi_eta_AntiKt4LCTopoJets.Fill(jets[Njet]["eta"],jets[Njet]["phi"])
            
        
        # Ordering jets by DeltaR

        
        # if(len(jets_sorted_run3) != len(jets_sorted_run2)):
            # starting=max(jets_sorted_run2)
            # for i in range(len(jets_sorted_run2)-len(jets_sorted_run3))
            # starting+=1
            # jets_sorted_run2.append(starting)
            # jets_run2[starting] = {"eta":99,"phi":99,"et":0} 


        jets_sorted_run2_matched = []
        jets_sorted_run3_matched = []

        for j in range(len(jets_sorted_run2)):
            
            Eta_Run2 = jets_run2[jets_sorted_run2[j]]["eta"]
            Phi_Run2 = jets_run2[jets_sorted_run2[j]]["phi"]      
            for i in range(len(jets_sorted_run3)):
                
                Eta_Run3 = jets_run3[jets_sorted_run3[i]]["eta"]
                Phi_Run3 = jets_run3[jets_sorted_run3[i]]["phi"]                   
                DeltaR = math.sqrt(pow(Eta_Run3-Eta_Run2,2)+pow(Phi_Run3-Phi_Run2,2))
                if(DeltaR<0.2):
                    skip=0
                    for val in jets_sorted_run2_matched:
                        if(val == jets_sorted_run2[j]):
                            skip = 1
                    if(skip): continue
                    jets_sorted_run2_matched.append(jets_sorted_run2[j])
                    jets_sorted_run3_matched.append(jets_sorted_run3[i])
                    
                    
        # if(len(jets_sorted_run2) != len(jets_sorted_run2_matched)):
        print("Run3: ",jets_sorted_run3)
        print("Run2: ",jets_sorted_run2)
        print(jets_sorted_run2_matched)
        print(jets_sorted_run3_matched)
        
        Njets_run2 += len(jets_sorted_run2)
        Njets_matched_run3 += len(jets_sorted_run3_matched)
        
        Nlarge = min(len(jets_sorted_run3_matched),len(jets_sorted_run2_matched))
        
        for i in range(Nlarge):
            # print(jets_run3[jets_sorted_run3[i]]["eta"])
            # print(jets_run2[jets_sorted_run2[i]]["eta"])
            h_eta_eta_Run3_Run2.Fill(jets_run3[jets_sorted_run3_matched[i]]["eta"],jets_run2[jets_sorted_run2_matched[i]]["eta"])
            h_phi_phi_Run3_Run2.Fill(jets_run3[jets_sorted_run3_matched[i]]["phi"],jets_run2[jets_sorted_run2_matched[i]]["phi"])
            h_phi_eta_glo_float.Fill(jets_run3[jets_sorted_run3_matched[i]]["eta"],jets_run3[jets_sorted_run3_matched[i]]["phi"])
            h_phi_eta_LVL1JetRoIs.Fill(jets_run2[jets_sorted_run2_matched[i]]["eta"],jets_run2[jets_sorted_run2_matched[i]]["phi"])
            
        print()
        print()
            
        # break
            
            
            
    print("Number of jets in Run2 : ",Njets_run2)
    print("Number of jets matched : ",Njets_matched_run3)
    print("Efficiency Run2/matched: ",format(Njets_matched_run3/Njets_run2*100,"5.2f"),"%")
            
            
            
            
            
            
            
            
            
#   Plotting and formatting below
#   1D plots
    h_tobeta.SetTitle("Local FPGA Eta Coordinates (LR Jets)")
    h_tobeta.GetXaxis().SetTitle("#eta values")
    h_tobeta.GetYaxis().SetTitle("No. TOBs")
    h_tobeta.SetMarkerStyle(3)
    h_tobeta = SetHisto(h_tobeta, "Local FPGA Eta Coordinates (LR Jets)", "#eta values")
    # c1 = createCanvasPads("1")
    # DrawCanvas(c1,h_tobeta, "")
#    c1.SaveAs("localtobEta_LR.png")

    h_tobphi.SetTitle("Local FPGA Phi Coordinates (LR Jets)")
    h_tobphi.GetXaxis().SetTitle("#phi values")
    h_tobphi.GetYaxis().SetTitle("No. TOBs")
    h_tobphi.SetMarkerStyle(3)
    h_tobphi = SetHisto(h_tobphi, "Local FPGA Phi Coordinates (LR Jets)", "#phi values")
    # c2 = createCanvasPads("2")
    # DrawCanvas(c2, h_tobphi, "")
#    c2.SaveAs("localtobPhi_LR.png")

    h_tobEt.SetTitle("TOB Et values, (LR Jets)")
    h_tobEt.GetXaxis().SetTitle("Et values, 200 MeV Steps")
    h_tobEt.GetYaxis().SetTitle("No. TOBs")
    h_tobEt = SetHisto(h_tobEt, "TOB Et values (LR Jets)", "Et values")                                
    # c3 = createCanvasPads("3")
    # DrawCanvas(c3,h_tobEt, "SAME")
    # c3.SaveAs("tobET_LR.png")

    h_tobSat.SetTitle("TOB Saturation values (LR Jets)")
    h_tobSat.GetXaxis().SetTitle("Saturation flag")
    h_tobSat.GetYaxis().SetTitle("No. TOBs")
    h_tobSat = SetHisto(h_tobSat, "TOB Saturation values (LR Jets)", "Saturation flag")
    # c4 = createCanvasPads("4")
    # DrawCanvas(c4,h_tobSat, "SAME")
    # c4.SaveAs("tobSat_LR.png")

    h_phi.SetTitle("Global Phi coordinates (LR Jets)")
    h_phi.GetXaxis().SetTitle("#phi values")
    h_phi.GetYaxis().SetTitle("No. TOBs")
    h_phi = SetHisto(h_phi, "Global #phi coordinates", "Global #phi values")                                
    # c5 = createCanvasPads("5")
    # DrawCanvas(c5,h_phi, "SAME")
#    c5.SaveAs("globalPhi_LR.png")

    h_eta.SetTitle("Global Eta coordinates (LR Jets)")
    h_eta.GetXaxis().SetTitle("#eta values")
    h_eta.GetYaxis().SetTitle("No. TOBs")
    h_eta = SetHisto(h_eta, "Global #eta coordinates", "Global #eta values")                                 
    # c6 = createCanvasPads("6")
    # DrawCanvas(c6,h_eta, "SAME")
#    c6.SaveAs("globalEta_LR.png")

    h_Et.SetTitle("Et values, unscaled (LR Jets)")
    h_Et.GetXaxis().SetTitle("Et values")
    h_Et.GetYaxis().SetTitle("No. TOBs")
    h_Et = SetHisto(h_Et, "Et values, unscaled", "Et values")                                 
    # c7 = createCanvasPads("7")
    # DrawCanvas(c7,h_Et, "SAME")
#    c7.SaveAs("Et_LR.png")

    h_phi_float.SetTitle("Global Phi float coordinates (LR Jets)")
    h_phi_float.GetXaxis().SetTitle("#phi values")
    h_phi_float.GetYaxis().SetTitle("No. TOBs")
    h_phi_float = SetHisto(h_phi_float, "Global #phi float coordinates", "Global #phi float values")
    # c8 = createCanvasPads("8")
    # DrawCanvas(c8,h_phi_float, "SAME")
#    c8.SaveAs("globalPhi_float_LR.png")

    h_eta_float.SetTitle("Global Eta float coordinates (LR Jets)")
    h_eta_float.GetXaxis().SetTitle("#eta values")
    h_eta_float.GetYaxis().SetTitle("No. TOBs")
    h_eta_float = SetHisto(h_eta_float, "Global #eta float coordinates", "Global #eta float values")
    # c9 = createCanvasPads("9")
    # DrawCanvas(c9,h_eta_float, "SAME")
#    c9.SaveAs("globalEta_float_LR.png")

#  2D plots

    h_phi_eta_glo_int.SetTitle("2D Global Phi-Eta Distribution (ints) - LR Jets")
    h_phi_eta_glo_int.GetXaxis().SetTitle("#eta values")
    h_phi_eta_glo_int.GetYaxis().SetTitle("#phi values")   
    h_phi_eta_glo_int.SetMarkerStyle(3)                  
    # c10 = createCanvasPads("10")
    # DrawCanvas(c10,h_phi_eta_glo_int, "")
#    c10.SaveAs("globalEtaPhi_int_LR.png")

    h_phi_eta_glo_float.SetTitle("2D Global Phi-Eta Distribution (floats) - SR Jets")
    h_phi_eta_glo_float.GetXaxis().SetTitle("#eta values")
    h_phi_eta_glo_float.GetYaxis().SetTitle("#phi values")
    h_phi_eta_glo_float.SetMarkerStyle(3)
    c11 = createCanvasPads("11")
    DrawCanvas(c11,h_phi_eta_glo_float, "")
#    c11.SaveAs("globalEtaPhi_floats_LR.png")

    h_phi_eta_local.SetTitle("2D Local Phi-Eta Distribution (ints) - LR Jets")
    h_phi_eta_local.GetXaxis().SetTitle("#eta values")
    h_phi_eta_local.GetYaxis().SetTitle("#phi values")
    h_phi_eta_local.SetMarkerStyle(3)
    # c12 = createCanvasPads("12")
    # DrawCanvas(c12,h_phi_eta_local, "")
#    c12.SaveAs("localEtaPhi_dist_int_LR.png")

    h_phi_eta_intimeantikt.SetTitle("2D Antikt Distribution (floats)")
    h_phi_eta_intimeantikt.GetXaxis().SetTitle("#eta values")
    h_phi_eta_intimeantikt.GetYaxis().SetTitle("#phi values")
    h_phi_eta_intimeantikt.SetMarkerStyle(4)
    h_phi_eta_intimeantikt.SetMarkerColor(2)
    # c13 = createCanvasPads("13")
    # DrawCanvas(c11,h_phi_eta_intimeantikt, "SAME")

    h_phi_eta_LVL1JetRoIs.SetTitle("2D Antikt Distribution (floats)")
    h_phi_eta_LVL1JetRoIs.GetXaxis().SetTitle("#eta values")
    h_phi_eta_LVL1JetRoIs.GetYaxis().SetTitle("#phi values")
    h_phi_eta_LVL1JetRoIs.SetMarkerStyle(4)
    h_phi_eta_LVL1JetRoIs.SetMarkerColor(3)
    # c13 = createCanvasPads("13")
    DrawCanvas(c11,h_phi_eta_LVL1JetRoIs, "SAME")

    h_phi_eta_AntiKt4EMPFlowJets.SetTitle("2D Antikt Distribution (floats)")
    h_phi_eta_AntiKt4EMPFlowJets.GetXaxis().SetTitle("#eta values")
    h_phi_eta_AntiKt4EMPFlowJets.GetYaxis().SetTitle("#phi values")
    h_phi_eta_AntiKt4EMPFlowJets.SetMarkerStyle(4)
    h_phi_eta_AntiKt4EMPFlowJets.SetMarkerColor(4)
    # c13 = createCanvasPads("13")
    # DrawCanvas(c11,h_phi_eta_AntiKt4EMPFlowJets, "SAME")

    h_phi_eta_AntiKt4EMTopoJets.SetTitle("2D Antikt Distribution (floats)")
    h_phi_eta_AntiKt4EMTopoJets.GetXaxis().SetTitle("#eta values")
    h_phi_eta_AntiKt4EMTopoJets.GetYaxis().SetTitle("#phi values")
    h_phi_eta_AntiKt4EMTopoJets.SetMarkerStyle(4)
    h_phi_eta_AntiKt4EMTopoJets.SetMarkerColor(6)
    # c13 = createCanvasPads("13")
    # DrawCanvas(c11,h_phi_eta_AntiKt4EMTopoJets, "SAME")

    h_phi_eta_AntiKt4LCTopoJets.SetTitle("2D Antikt Distribution (floats)")
    h_phi_eta_AntiKt4LCTopoJets.GetXaxis().SetTitle("#eta values")
    h_phi_eta_AntiKt4LCTopoJets.GetYaxis().SetTitle("#phi values")
    h_phi_eta_AntiKt4LCTopoJets.SetMarkerStyle(4)
    h_phi_eta_AntiKt4LCTopoJets.SetMarkerColor(7)
    # c13 = createCanvasPads("13")
    # DrawCanvas(c11,h_phi_eta_AntiKt4LCTopoJets, "SAME")
#    c11.SaveAs("globalEtaPhi_floats_LR.png")


    leg = TLegend(.84,.65,.99,.85)
    # leg.SetBorderSize(0)
    # leg.SetFillColor(0)
    # leg.SetFillStyle(0)
    # leg.SetTextFont(42)
    # leg.SetTextSize(0.035)
    leg.AddEntry(h_phi_eta_glo_float,"Small-R Jet EDM","P")
    # leg.AddEntry(h_phi_eta_intimeantikt,"InTimeAntiKt4TruthJets","P")
    leg.AddEntry(h_phi_eta_LVL1JetRoIs,"LVL1JetRoIs","P")
    # leg.AddEntry(h_phi_eta_AntiKt4EMPFlowJets,"AntiKt4EMPFlowJets","P")
    # leg.AddEntry(h_phi_eta_AntiKt4EMTopoJets,"AntiKt4EMTopoJets","P")
    # leg.AddEntry(h_phi_eta_AntiKt4LCTopoJets,"AntiKt4LCTopoJets","P")
    DrawCanvas(c11,leg, "SAME")
    
    
    h_eta_eta_Run3_Run2.SetTitle("Run3 vs. Run2 SR-Jets")
    h_eta_eta_Run3_Run2.GetXaxis().SetTitle("#eta Run3 values")
    h_eta_eta_Run3_Run2.GetYaxis().SetTitle("#eta Run2 values")
    h_eta_eta_Run3_Run2.SetMarkerStyle(3)
    c12 = createCanvasPads("12")
    DrawCanvas(c12,h_eta_eta_Run3_Run2, "")
        
    h_phi_phi_Run3_Run2.SetTitle("Run3 vs. Run2 SR-Jets")
    h_phi_phi_Run3_Run2.GetXaxis().SetTitle("#phi Run3 values")
    h_phi_phi_Run3_Run2.GetYaxis().SetTitle("#phi Run2 values")
    h_phi_phi_Run3_Run2.SetMarkerStyle(3)
    c13 = createCanvasPads("13")
    DrawCanvas(c13,h_phi_phi_Run3_Run2, "")    
    
    
    
    
    
    

"""
    h_Eta = SetHisto(h_Eta, "Global #eta values of LR", "Global #eta values")                                
    c2 = createCanvasPads("2")
    DrawCanvas(c2,h_Eta, "SAME")
    c2.SaveAs("globalEta_LR_EDM.png")

    #h_Phi = SetHisto(h_Phi, "Global #phi values of LR", "Global #phi values")                                
#    c3 = createCanvasPads("3")
#    DrawCanvas(c3,h_Phi, "SAME")
#    c3.SaveAs("globalPhi_LR_evt1_EDM.png")
    
    #h_phi_eta.SetTitle("2D Local Phi-Eta Distribution - LR Jets")
    #h_phi_eta.GetXaxis().SetTitle("#eta values")
    #h_phi_eta.GetYaxis().SetTitle("#phi values")   
    #h_phi_eta.SetMarkerStyle(3)                  
#    c4 = createCanvasPads("4")
#    DrawCanvas(c4,h_phi_eta, "")
#    c4.SaveAs("localEtaPhi_LR_fpga0EDM.png") 

    h_phi_eta_glo_int.SetTitle("2D Global Phi-Eta Distribution (ints) - LR Jets")
    h_phi_eta_glo_int.GetXaxis().SetTitle("#eta values")
    h_phi_eta_glo_int.GetYaxis().SetTitle("#phi values")   
    h_phi_eta_glo_int.SetMarkerStyle(3)                  
    c5 = createCanvasPads("5")
    DrawCanvas(c5,h_phi_eta_glo_int, "")
    c5.SaveAs("globalEtaPhi_dist_int.png")

    h_phi_eta_glo_float.SetTitle("2D Global Phi-Eta Distribution (floats) - LR Jets")
    h_phi_eta_glo_float.GetXaxis().SetTitle("#eta values")
    h_phi_eta_glo_float.GetYaxis().SetTitle("#phi values")
    h_phi_eta_glo_float.SetMarkerStyle(3)
    c6 = createCanvasPads("5")
    DrawCanvas(c6,h_phi_eta_glo_float, "")
    c6.SaveAs("globalEtaPhi_dist_floats.png")

    h_phi_eta_local.SetTitle("2D Local Phi-Eta Distribution (ints) - LR Jets")
    h_phi_eta_local.GetXaxis().SetTitle("#eta values")
    h_phi_eta_local.GetYaxis().SetTitle("#phi values")
    h_phi_eta_local.SetMarkerStyle(3)
#    c7 = createCanvasPads("7")
#    DrawCanvas(c7,h_phi_eta_local, "")
#    c7.SaveAs("localEtaPhi_dist_int.png")

#    h_phi_local= SetHisto(h_phi_local, "Local #Phi values of LR", "Local #Phi values")                                
#    c6 = createCanvasPads("6")
#    DrawCanvas(c6,h_phi_local, "SAME")
#    c6.SaveAs("localPhi_LR_EDM_barrel.png")

    c8 = createCanvasPads("8")
    DrawCanvas(c8,h_phi,"")
    c8.SaveAs("globalphi.png")

    c9 = createCanvasPads("9")
    DrawCanvas(c9,h_fpga,"")
    c9.SaveAs("fpga_dist.png")

    print("Done..")
"""



def createCanvasPads(name_):
    c = TCanvas( name_, 'Dynamic Filling', 1500,1000 )
    c.SetTopMargin(0.07)
    c.SetRightMargin(0.15)
    c.SetLeftMargin(0.10)
    return c

def SetHisto(h,title, xTitle=""):
  h.SetTitle(title)
  h.GetXaxis().SetTitle(xTitle)
  h.GetYaxis().SetTitle("Number of TOBs")
#  h.GetZaxis().SetTitle("Et (scale 200 MeV) [MeV]")
  #h_eta_v_phi_map.GetYaxis().SetNdivisions(0,1,0);
  h.SetLineColor(2)
  h.SetFillStyle(3325)
  h.SetFillColor(2)
  h.SetLineWidth(1)
  h.SetStats(0)
  gStyle.SetOptStat(10);
  return h

def DrawCanvas(c,h,opt=""):
    c.SetBatch(ROOT.kFALSE)
    c.cd()   
    # h.Draw("COLZ"+opt)
    h.Draw(opt)
    gPad.RedrawAxis()
    c.Modified()
    c.Update()

def main(): 
    ReadTOBs("myfile_jfex.root")
    input()

if __name__ == '__main__':   
    main()

