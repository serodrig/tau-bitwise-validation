#!/usr/bin/env python                                                                                                                                                                 
# -*- coding: utf-8 -*-         
import ROOT
ROOT.gROOT.SetBatch(True)
from ROOT import xAOD
from ROOT import TTree, TChain, TF1, TH1I, TH1F, TH1D, TH2F, TH2I, TH2D, TGraphErrors, TGraph, THStack, TLegend, TGraph2D, TLatex
from ROOT import TFile, TCanvas, TPad, TLine, TLorentzVector
from ROOT import gPad,gStyle
import array as arr


def ReadTOBs():
    
    xAOD.Init()

    # Set up the input files
    fileName = '../xAOD.jFEX.output.root'
    treeName = 'CollectionTree'
    f = TFile.Open(fileName)

    # Make the "transient tree"
    event = ROOT.xAOD.TEvent (ROOT.xAOD.TEvent.kAthenaAccess)
    event.readFrom (f, True, 'CollectionTree')    
    t = xAOD.MakeTransientTree(event, treeName)
    
    # Print some information
    print('Number of input events: %s' % t.GetEntries())

    # loop over electron collection
    for entry in range(t.GetEntries()):
    # for entry in range(1):
        t.GetEntry(entry)
        # t.GetEntry(11)
        s=" event %4d "%(entry)
        s="*"*50+s+"*"*50 
        # print(s)
        # print("-"*10+" jTAU "+"-"*10)
        for elem in t.L1_jFexTauRoI:
            
            s="jFEX: %1d   FPGA: %1d - iEta(eta): %2d(%6.2f) iPhi(phi): %2d(%6.2f) Et: %4d Iso: %4d Sat: %1d   ---> TOB: %15d" % (elem.jFexNumber(),elem.fpgaNumber(),elem.tobLocalEta(),elem.eta(),elem.tobLocalPhi(),elem.phi(),elem.tobEt(),elem.tobIso(),elem.tobSat(),elem.tobWord())
            # print(s)
         
        # print("-"*10+" jJ "+"-"*10)   
        for elem in t.L1_jFexSRJetRoI:
            
            # s="jFEX: %1d   FPGA: %1d - iEta(eta): %2d(%6.2f) iPhi(phi): %2d(%6.2f) Et: %4d Sat: %1d   ---> TOB: %15d" % (elem.jFexNumber(),elem.fpgaNumber(),elem.tobLocalEta(),elem.eta(),elem.tobLocalPhi(),elem.phi(),elem.tobEt(),elem.tobSat(),elem.tobWord())
            s="jFEX: %1d   FPGA: %1d - iEta(eta): %2d(%3d/%6.2f) iPhi(phi): %2d(%3d/%6.2f) Et: %4d Sat: %1d   ---> TOB: %15d" % (elem.jFexNumber(),elem.fpgaNumber(),elem.tobLocalEta(),elem.globalEta(),elem.eta(),elem.tobLocalPhi(),elem.globalPhi(),elem.phi(),elem.tobEt(),elem.tobSat(),elem.tobWord())
            if(elem.jFexNumber()!=1 or elem.fpgaNumber()!=0 ): continue
            # if(elem.globalPhi()>25 or elem.globalPhi()<20): continue
            print(entry, s)
         
        # print("-"*10+" jLJ "+"-"*10)   
        for elem in t.L1_jFexLRJetRoI:
            
            s="jFEX: %1d   FPGA: %1d - iEta(eta): %2d(%6.2f) iPhi(phi): %2d(%6.2f) Et: %4d Sat: %1d   ---> TOB: %15d" % (elem.jFexNumber(),elem.fpgaNumber(),elem.tobLocalEta(),elem.eta(),elem.tobLocalPhi(),elem.phi(),elem.tobEt(),elem.tobSat(),elem.tobWord())
            # print(s)
            
        # print("-"*10+" jXE "+"-"*10)   
        for elem in t.L1_jFexMETRoI:
            
            s="jFEX: %1d   FPGA: %1d - Ex: %6d Ey: %6d Sat: %2d Res: %2d   ---> TOB: %15d" % (elem.jFexNumber(),elem.fpgaNumber(),elem.tobEx(),elem.tobEy(),elem.tobSat(),elem.tobRes(),elem.tobWord())
            # print(s)
            
        # print("-"*10+" jTE "+"-"*10)   
        for elem in t.L1_jFexSumETRoI:
            
            s="jFEX: %1d   FPGA: %1d - Et_lower: %6d Sat_lower: %2d Et_upper: %6d Sat_upper: %2d ---> TOB: %15d" % (elem.jFexNumber(),elem.fpgaNumber(),elem.tobEt_lower(),elem.tobSat_lower(),elem.tobEt_upper(),elem.tobSat_upper(),elem.tobWord())
            # print(s)

    print("Done..")


def main(): 

    ReadTOBs()

if __name__ == '__main__':   
    main()
