#!/usr/bin/env python                                                                                                                                                                 
# -*- coding: utf-8 -*-   
import os
import ROOT
ROOT.gROOT.SetBatch(True)
from ROOT import xAOD
from ROOT import TTree, TChain, TF1, TH1I, TH1F, TH1D, TH2F, TH2I, TH2D, TGraphErrors, TGraph, THStack, TLegend, TGraph2D, TLatex, TMath
from ROOT import TFile, TCanvas, TPad, TLine, TLorentzVector
from ROOT import gPad,gStyle
import array as arr


def ReadTOBs(_dir_):
    
    xAOD.Init()

    # Set up the input files
    fileName = '../xAOD.jFEX.output.root'
    treeName = 'CollectionTree'
    f = TFile.Open(fileName)

    # Make the "transient tree"
    event = ROOT.xAOD.TEvent (ROOT.xAOD.TEvent.kAthenaAccess)
    event.readFrom (f, True, 'CollectionTree')
    t = ROOT.xAOD.MakeTransientTree(event, 'CollectionTree')    
    # t = xAOD.MakeTransientTree(f, treeName)
    
    # Print some information
    print('Number of input events: %s' % t.GetEntries())


    # Defining histos
    h_tobEt_lo       = TH1F( 'tobEt_low' , 'tobEt_low' , 250, 0, 5 )
    h_tobEt_up       = TH1F( 'tobEt_up'  , 'tobEt_up'  , 250, 0, 5 )
    h_Et_Cside       = TH1F( 'Et_Cside'  , 'Et_Cside'  , 250, 0, 5 )
    h_Et_Aside       = TH1F( 'Et_Aside'  , 'Et_Aside'  , 250, 0, 5 )
    h_Et_jfex_0      = TH1F( 'Et_jfex_0' , 'Et_jfex_0' , 250, 0, 5 )
    h_Et_jfex_1      = TH1F( 'Et_jfex_1' , 'Et_jfex_1' , 250, 0, 5 )
    h_Et_jfex_2      = TH1F( 'Et_jfex_2' , 'Et_jfex_2' , 250, 0, 5 )
    h_Et_jfex_3      = TH1F( 'Et_jfex_3' , 'Et_jfex_3' , 250, 0, 5 )
    h_Et_jfex_4      = TH1F( 'Et_jfex_4' , 'Et_jfex_4' , 250, 0, 5 )
    h_Et_jfex_5      = TH1F( 'Et_jfex_5' , 'Et_jfex_5' , 250, 0, 5 )
    h_SumEt          = TH1F( 'SumEt'     , 'SumEt'     , 140, 0, 70)

    # loop over electron collection
    for entry in range(t.GetEntries()):
        t.GetEntry(entry)
        
        Total_sumET = 0
        for elem in t.L1_jFexSumETRoI:
            
            s="Module: %1d   FPGA: %1d - Et_lower: %6d Sat_lower: %2d Et_hihger: %6d Sat_upper: %2d   ---> TOB: %15d" % (elem.jFexNumber(),elem.fpgaNumber(),elem.tobEt_lower(),elem.tobSat_lower(),elem.tobEt_upper(),elem.tobSat_upper(),elem.tobWord())
            # print(s)
            summedET = elem.tobEt_lower()+elem.tobEt_upper()
            
            h_tobEt_lo.Fill(elem.tobEt_lower()/1e3,1)
            h_tobEt_up.Fill(elem.tobEt_upper()/1e3,1)
            
            if(elem.jFexNumber()==0):
                h_Et_Cside.Fill(summedET/1e3,1)
                h_Et_jfex_0.Fill(summedET/1e3,1)
                
            elif(elem.jFexNumber()==1):
                h_Et_Cside.Fill(summedET/1e3,1)
                h_Et_jfex_1.Fill(summedET/1e3,1)
                
            elif(elem.jFexNumber()==2):
                h_Et_Cside.Fill(summedET/1e3,1)
                h_Et_jfex_2.Fill(summedET/1e3,1)
                
            elif(elem.jFexNumber()==3):
                h_Et_Aside.Fill(summedET/1e3,1)
                h_Et_jfex_3.Fill(summedET/1e3,1)
                
            elif(elem.jFexNumber()==4):
                h_Et_Aside.Fill(summedET/1e3,1)
                h_Et_jfex_4.Fill(summedET/1e3,1)
                
            elif(elem.jFexNumber()==5):
                h_Et_Aside.Fill(summedET/1e3,1)
                h_Et_jfex_5.Fill(summedET/1e3,1)
                
            else:
                print("This should never happen")
            Total_sumET += summedET
            
        h_SumEt.Fill(Total_sumET/1e3 ,1)

    h_tobEt_lo = SetHisto(h_tobEt_lo, "TobEt_lower values of jTE", "tob Et_lower [GeV/200] ")                                
    c1 = createCanvasPads("1")
    DrawCanvas(c1,h_tobEt_lo, "SAME")

    h_tobEt_up = SetHisto(h_tobEt_up, "TobEt_upper values of jTE", "tob Et_upper [GeV/200] ")                                
    c2 = createCanvasPads("2")
    DrawCanvas(c2,h_tobEt_up, "SAME")

    h_Et_Cside = SetHisto(h_Et_Cside, "Et C side for jTE", "SumEt in C sice [GeV/200] ")                                
    c3 = createCanvasPads("3")
    DrawCanvas(c3,h_Et_Cside, "SAME")

    h_Et_Aside = SetHisto(h_Et_Aside, "Et A side for jTE", "SumEt in A sice [GeV/200] ")                                
    c4 = createCanvasPads("4")
    DrawCanvas(c4,h_Et_Aside, "SAME")

    h_Et_jfex_0 = SetHisto(h_Et_jfex_0, "SumEt values of jTE", "SumEt [GeV/200] in jFEX 0 ")                                
    c5 = createCanvasPads("5")
    DrawCanvas(c5,h_Et_jfex_0, "SAME")

    h_Et_jfex_1 = SetHisto(h_Et_jfex_1, "SumEt values of jTE", "SumEt [GeV/200] in jFEX 1 ")                                
    c6 = createCanvasPads("6")
    DrawCanvas(c6,h_Et_jfex_1, "SAME")

    h_Et_jfex_2 = SetHisto(h_Et_jfex_2, "SumEt values of jTE", "SumEt [GeV/200] in jFEX 2 ")                                
    c7 = createCanvasPads("7")
    DrawCanvas(c7,h_Et_jfex_2, "SAME")

    h_Et_jfex_3 = SetHisto(h_Et_jfex_3, "SumEt values of jTE", "SumEt [GeV/200] in jFEX 3 ")                                
    c8 = createCanvasPads("8")
    DrawCanvas(c8,h_Et_jfex_3, "SAME")

    h_Et_jfex_4 = SetHisto(h_Et_jfex_4, "SumEt values of jTE", "SumEt [GeV/200] in jFEX 4 ")                                
    c9 = createCanvasPads("9")
    DrawCanvas(c9,h_Et_jfex_4, "SAME")

    h_Et_jfex_5 = SetHisto(h_Et_jfex_5, "SumEt values of jTE", "SumEt [GeV/200] in jFEX 5 ")                                
    c10 = createCanvasPads("10")
    DrawCanvas(c10,h_Et_jfex_5, "SAME")

    h_SumEt = SetHisto(h_SumEt, "jTE Et [GeV/200]", "Total jTE Et [GeV/200] ")
    c11 = createCanvasPads("11")
    c11.SetLogy() 
    DrawCanvas(c11,h_SumEt, "SAME")

    c1 .SaveAs(_dir_+"/SumET_plots/tobEt_low.png")
    c2 .SaveAs(_dir_+"/SumET_plots/tobEt_up.png")
    c3 .SaveAs(_dir_+"/SumET_plots/Et_Cside.png")
    c4 .SaveAs(_dir_+"/SumET_plots/Et_Aside.png")
    c5 .SaveAs(_dir_+"/SumET_plots/Et_jfex_0.png")
    c6 .SaveAs(_dir_+"/SumET_plots/Et_jfex_1.png")
    c7 .SaveAs(_dir_+"/SumET_plots/Et_jfex_2.png")
    c8 .SaveAs(_dir_+"/SumET_plots/Et_jfex_3.png")
    c9 .SaveAs(_dir_+"/SumET_plots/Et_jfex_4.png")
    c10.SaveAs(_dir_+"/SumET_plots/Et_jfex_5.png")
    c11.SaveAs(_dir_+"/SumET_plots/SumEt.png")
    
    outfile = TFile( _dir_+"/SumET_plots/SumET_file.root", 'RECREATE' )
    h_tobEt_lo.Write()
    h_tobEt_up.Write()
    h_Et_Cside.Write()
    h_Et_Aside.Write()
    h_Et_jfex_0.Write()
    h_Et_jfex_1.Write()
    h_Et_jfex_2.Write()
    h_Et_jfex_3.Write()
    h_Et_jfex_4.Write()
    h_Et_jfex_5.Write()
    h_SumEt.Write()
    outfile.Close()    
    print("Done..")
   
    
    
def createCanvasPads(name_):
    c = TCanvas( name_, 'Dynamic Filling', 1500,1000 )
    c.SetTopMargin(0.07)
    c.SetRightMargin(0.10)
    c.SetLeftMargin(0.10)

    return c

def SetHisto(h,title, xTitle="", show = False):
    h.SetTitle(title)
    h.GetXaxis().SetTitle(xTitle)
    h.GetYaxis().SetTitle("Number of TOBs")
    h.GetYaxis().SetTitleOffset(1)
    # h.GetZaxis().SetTitle("Et (scale 200 MeV) [MeV]")
    # h_eta_v_phi_map.GetYaxis().SetNdivisions(0,1,0);
    h.SetLineColor(2)
    h.SetFillStyle(3002)
    h.SetFillColor(2)
    h.SetLineWidth(1)
    if(show):
        gStyle.SetOptStat(10)
    else:
        h.SetStats(0)
    
    return h

def DrawCanvas(c,h,opt=""):
    c.SetBatch(ROOT.kFALSE)
    c.cd()   
    # h.Draw("COLZ"+opt)
    h.Draw(opt)
    gPad.RedrawAxis()
    c.Modified()
    c.Update()

def main(): 
    
    from optparse import OptionParser 
    parser = OptionParser(usage = "usage: %prog arguments", version="%prog") 
    parser.add_option('-d','--directory',action="store", type="string", dest='folder', help='Main directory of the validation  (default: %default)')
    parser.set_defaults(folder="Non")
    (options,args) = parser.parse_args()
    os.system("mkdir -p "+ options.folder+"/SumET_plots")
    ReadTOBs(options.folder)

if __name__ == '__main__':   
    main()
