#!/usr/bin/env python                                                                                                                                                                 
# -*- coding: utf-8 -*-                                                                                                                                                               
import os,sys
import ROOT
# ROOT.gROOT.SetBatch(True)
from array import array
import inspect
import random                                                                                                                                   
#import StringIO                                                                                                                                     
from ROOT import gROOT
import json

from ROOT import TTree, TChain, TF1, TH1I, TH1F, TH1D, TH2F, TH2D, TGraphErrors, TGraph, THStack, TLegend, TGraph2D, TLatex
from ROOT import TFile, TCanvas, TPad, TLine, TLorentzVector
from ROOT import gPad,gStyle

#gStyle.SetPalette(ROOT.kRainBow);
gStyle.SetNumberContours(255);                                                     

#sys.path.append('../run/lib')
#from AtlasStyle import AtlasStyle
#folder ="/home/serobos/Desktop/"
folder ="../"

def main(): 
    from optparse import OptionParser 
    parser = OptionParser(usage = "usage: %prog arguments", version="%prog") 
    parser.add_option('-e','--entry',action="store", type="int", dest='entry', help='input file txt  (default: %default)')
    #parser.add_option('-o','--option', dest='option',                               help='input directory with csv files (default: %default)')
    parser.set_defaults(entry=0)
    (options,args) = parser.parse_args()



    #files = ["BDT0_0.15_3_20_1000.root", "BDT0_0.15_5_20_1000.root"]
    #val = ROC(files)
    print("Event: ",options.entry)
    plotting("myfile_jfex.root",options.entry)
    input()

def plotting(_file_,par_entry):
    
    #style = AtlasStyle()
    #gStyle.SetOptStat(0)
    #gStyle.SetPadRightMargin(0.09)
    
    
    #legend = TLegend(0.5, 0.7, 0.8, 0.9)
    #legend.SetLineWidth(0)
    #legend.SetLineColor(0)

    print("Opening file..")
    # print(folder + _file_)
    rootFile = TFile(folder + _file_)
    # print(rootFile.ls())
    tree = rootFile.Get("data")
    # print( tree)
    v_tau_ET = ROOT.vector('int')()
    v_tau_clusterET = ROOT.vector('int')()
    v_tau_eta = ROOT.vector('int')()
    v_tau_phi = ROOT.vector('int')()
    v_tau_realeta = ROOT.vector('int')()
    v_tau_TT_ID = ROOT.vector('int')()
    v_tau_isMax = ROOT.vector('int')()
    v_tau_jFEXid = ROOT.vector('int')()
    v_tau_FPGAid = ROOT.vector('int')()
    v_tau_TOB_word = ROOT.vector('uint32_t')()
    v_tau_iso = ROOT.vector('int')()
    v_tau_TOB_ET = ROOT.vector('int')()

    print("Setting up branches..")
    tree.SetBranchStatus('*',0)
    tree.SetBranchStatus('tau_*',1)
    tree.SetBranchAddress("tau_ET", v_tau_ET)
    tree.SetBranchAddress("tau_clusterET", v_tau_clusterET)
    tree.SetBranchAddress("tau_eta", v_tau_eta)
    tree.SetBranchAddress("tau_phi", v_tau_phi)
    tree.SetBranchAddress("tau_realeta", v_tau_realeta)
    tree.SetBranchAddress("tau_TT_ID", v_tau_TT_ID)
    tree.SetBranchAddress("tau_isLocalMax", v_tau_isMax)
    tree.SetBranchAddress("tau_jFEXid", v_tau_jFEXid)
    tree.SetBranchAddress("tau_FPGAid", v_tau_FPGAid)
    tree.SetBranchAddress("tau_TOB_word", v_tau_TOB_word)
    tree.SetBranchAddress("tau_TOB_ET", v_tau_TOB_ET)
    tree.SetBranchAddress("tau_ISO", v_tau_iso)
    

    print("Creating histos..")
    h_Et   = TH1F( 'et', 'et', 200, 0, 400 )
    h_Eta  = TH1F( 'eta', 'eta', 50, -25, 25)
    h_Phi  = TH1F( 'phi', 'phi', 65, 0, 65 )
    h_eta_v_phi_map  = TH2F( 'eta/phi', 'phi vs eta', 50, -25, 25, 64, 0, 64 )
    h_eta_v_phi_map_ET  = TH2F( 'eta/phi_et', 'phi vs eta', 50, -25, 25, 64, 0, 64  )
    h_eta_v_phi_map_localMAX  = TH2F( 'eta/phi_LM', 'phi vs eta', 50, -25, 25, 64, 0, 64  )
    h_eta_v_phi_map_Barrel0  = TH2F( 'h_eta_v_phi_map_Barrel0', 'h_eta_v_phi_map_Barrel0', 50, -25, 25, 64, 0, 64  )
    h_eta_v_phi_map_Barrel1  = TH2F( 'h_eta_v_phi_map_Barrel1', 'h_eta_v_phi_map_Barrel1', 50, -25, 25, 64, 0, 64  )
    h_eta_v_phi_map_Barrel2  = TH2F( 'h_eta_v_phi_map_Barrel2', 'h_eta_v_phi_map_Barrel2', 50, -25, 25, 64, 0, 64  )
    h_eta_v_phi_map_Barrel3  = TH2F( 'h_eta_v_phi_map_Barrel3', 'h_eta_v_phi_map_Barrel3', 50, -25, 25, 64, 0, 64  )
    h_eta_v_phi_map_Barrel4  = TH2F( 'h_eta_v_phi_map_Barrel4', 'h_eta_v_phi_map_Barrel4', 50, -25, 25, 64, 0, 64  )
    h_eta_v_phi_map_Barrel5  = TH2F( 'h_eta_v_phi_map_Barrel5', 'h_eta_v_phi_map_Barrel5', 50, -25, 25, 64, 0, 64  )
    h_TTid_map  = TH2F( 'h_TTid_map', 'phi vs eta', 50, -25, 25, 64, 0, 64  )
    
    xbins = [-32,-31,-29,-27,-25,25,27,29,31,32]
    h_eta_v_phi_map_ET_1 = TH2F( 'eta/phi_et1', 'phi vs eta', len(xbins)-1, array('d',xbins), 16, 0, 64  )

    xbins  = [0]*24
    xlow=-49.0
    for i in range(len(xbins)//2):
      xbins[i]=round(xlow,2)
      xlow+=1.5454

    xlow=32.0
    for i in range(len(xbins)//2):
      xbins[i+12]=round(xlow,2)
      xlow+=1.5454

    # print(xbins)

    
    h_eta_v_phi_map_ET_2 = TH2F( 'eta/phi_et2', 'phi vs eta', len(xbins)-1, array('d',xbins), 8, 0, 64  )
    h_tau_ET    = TH1F( 'h_tau_ET', 'h_tau_ET', 85, 0, 17750 )



    module_tobs = [[{},{},{},{}],[{},{},{},{}],[{},{},{},{}],[{},{},{},{}],[{},{},{},{}],[{},{},{},{}]]
    # print("Looping over entries..")
    # print("Taus above 30000 MeV:")
    
    # for i in range(tree.GetEntries()):
    tree.GetEntry(par_entry)

    order = 0
    ntaus = 0
    for j in range(v_tau_ET.size()):
        if(v_tau_ET[j]>30000):
            ntaus +=1 
            # print("Number: ",ntaus,"Energy (MeV)=",v_tau_ET[j],"eta=",format(v_tau_realeta[j],"3d"),"phi=",format(v_tau_phi[j],"3d"), "---->  Is a local Maxima=",v_tau_isMax[j])
        add = 0.5
        if(int(str(v_tau_TT_ID[j])[0])%2!=0): add=add*(-1)

        if(v_tau_jFEXid[j]==1):
            h_eta_v_phi_map_Barrel1.Fill(v_tau_realeta[j]+add,v_tau_phi[j]+0.5,1 )
        if(v_tau_jFEXid[j]==2):
            h_eta_v_phi_map_Barrel2.Fill(v_tau_realeta[j]+add,v_tau_phi[j]+0.5,1 )
        if(v_tau_jFEXid[j]==3):
            h_eta_v_phi_map_Barrel3.Fill(v_tau_realeta[j]+add,v_tau_phi[j]+0.5,1 )
        if(v_tau_jFEXid[j]==4 or v_tau_jFEXid[j]==5 or v_tau_jFEXid[j]==0):
            h_eta_v_phi_map_Barrel4.Fill(v_tau_realeta[j]+add,v_tau_phi[j]+0.5,1 )
        
        if(v_tau_ET[j]==0): h_eta_v_phi_map_ET.Fill(v_tau_realeta[j]+add,v_tau_phi[j]+0.5,0.001 )
        # elif(v_tau_ET[j]>60000): h_eta_v_phi_map_ET.Fill(v_tau_realeta[j]+add,v_tau_phi[j]+0.5,0.001 )
        else: h_eta_v_phi_map_ET.Fill(v_tau_realeta[j]+add,v_tau_phi[j]+0.5,v_tau_ET[j]/200 )
        
        h_eta_v_phi_map.Fill(v_tau_realeta[j]+add,v_tau_phi[j]+0.5,v_tau_clusterET[j]/200 )

        if(v_tau_isMax[j]==1 ):

            module_tobs[v_tau_jFEXid[j]][v_tau_FPGAid[j]][order]=[v_tau_jFEXid[j], v_tau_FPGAid[j], v_tau_realeta[j]+add, v_tau_phi[j]+0.5, v_tau_clusterET[j]//200, v_tau_iso[j]//200, v_tau_TOB_word[j]]
            order+=1
            h_eta_v_phi_map_localMAX.Fill(v_tau_realeta[j]+add,v_tau_phi[j]+0.5,1 )
        


    print("Plotting..")
    h_eta_v_phi_map = SetHisto(h_eta_v_phi_map,"All Madule mapping validation")
    h_eta_v_phi_map_localMAX = SetHisto(h_eta_v_phi_map_localMAX,"Tau Local Maximas")
    h_eta_v_phi_map_ET = SetHisto(h_eta_v_phi_map_ET,"TOBs sent to L1Topo")
    h_eta_v_phi_map_Barrel1 = SetHisto(h_eta_v_phi_map_Barrel1,"Module 1 mapping validation")
    h_eta_v_phi_map_Barrel2 = SetHisto(h_eta_v_phi_map_Barrel2,"Module 2 mapping validation")
    h_eta_v_phi_map_Barrel3 = SetHisto(h_eta_v_phi_map_Barrel3,"Module 3 mapping validation")
    h_eta_v_phi_map_Barrel4 = SetHisto(h_eta_v_phi_map_Barrel4,"Module 4 mapping validation")
    h_eta_v_phi_map_ET_2 = SetHisto(h_eta_v_phi_map_ET_2,"Module 0/5 mapping validation")
    h_eta_v_phi_map_ET_1 = SetHisto(h_eta_v_phi_map_ET_1,"Module 0/5 regular mapping validation")

    c = createCanvasPads("1")
    DrawCanvas(c,h_eta_v_phi_map_localMAX)

    c1 = createCanvasPads("2")
    DrawCanvas(c1,h_eta_v_phi_map_ET)
      
    c2 = createCanvasPads("3")
    DrawCanvas(c2,h_eta_v_phi_map)
    
    c3 = createCanvasPads("4")
    DrawCanvas(c3,h_eta_v_phi_map_Barrel1)
    
    c4 = createCanvasPads("5")
    DrawCanvas(c4,h_eta_v_phi_map_Barrel2)
    
    c5 = createCanvasPads("6")
    DrawCanvas(c5,h_eta_v_phi_map_Barrel3)
    
    c6 = createCanvasPads("7")
    DrawCanvas(c6,h_eta_v_phi_map_Barrel4)

    
    for i in range(len(module_tobs)):
        for j in range(len(module_tobs[i])):
            module_tobs[i][j]= dict(sorted(module_tobs[i][j].items(), reverse=True, key=lambda item: item[1][4]))
            while len(module_tobs[i][j])>6:
                module_tobs[i][j].popitem()    

    """   

    print("-"*50,"Event entry ",1,"-"*50)
    for i in range(len(module_tobs)):
        for j in range(len(module_tobs[i])):  
            print("TOBs for the jFEX: ",i," and FPGA: ",j,"\n")
            print(format("Num","3s"), format("eta","3s"), ', ', format("phi","3s"), ', ', format(" Et","4s"), ' , ', format(" Iso","4s"), ' ---> ', format("TOB (32bits)","32s"),"  -  ", format( "TOB (decimal)","15s"))
            for key in module_tobs[i][j]:
                # print(format(key,"3d"), ' : ', format(module_tobs[i][j][key][0],"2d"), ' , ', format(module_tobs[i][j][key][1],"2d"), ' , ', format(module_tobs[i][j][key][2],"2d"), ' , ', format(module_tobs[i][j][key][3],"2d"), ' , ', format(module_tobs[i][j][key][4],"4d"), ' , ', format(module_tobs[i][j][key][5],"4d"), ' ---> ', module_tobs[i][j][key][6],"  -  ", format(module_tobs[i][j][key][7],"15d"))
                if(module_tobs[i][j][key][6]==0):
                    print(format(key,"3d"), format("-"*3,"3s"), ' , ', format("-"*2,"2s"), ' , ', format("-"*4,"4s"), ' , ',  format("-"*4,"4s"), ' ---> ', module_tobs[i][j][key][6],"  -  ")
                else: 
                    print(format(key,"3d"), format(module_tobs[i][j][key][2],"3.0f"), ' , ', format(module_tobs[i][j][key][3],"2.0f"), ' , ', format(module_tobs[i][j][key][4],"4.0f"), ' , ', format(module_tobs[i][j][key][5],"4.0f"), ' ---> ', format(module_tobs[i][j][key][6],"032b"),"  -  ",format(module_tobs[i][j][key][6],"15d") )
            print("\n\n")

    """

    
    
    PrintLM(c,h_eta_v_phi_map_localMAX,module_tobs)
    PrintLM(c1,h_eta_v_phi_map_localMAX,module_tobs)
    PrintLM(c2,h_eta_v_phi_map_localMAX,module_tobs)

    
    #c.SaveAs("plots/LocalMaximas_map.png")

    # c1.SaveAs("Tau_TOBs_phi_eta_plane.pdf")
    
    f = TFile("demo.root","recreate")
    c1.SetName("Entry_"+str(par_entry))
    c1.Write()
    f.Close()
    
    
    #c2.SaveAs("plots/SeedET_with_LM.png")
    #c3.SaveAs("plots/FPGA1mapping_validation.png")
    #c4.SaveAs("plots/FPGA2mapping_validation.png")
    #c5.SaveAs("plots/FPGA3mapping_validation.png")
    #c6.SaveAs("plots/FPGA4mapping_validation.png")

    h_eta_v_phi_map_Barrel4 = SetHisto(h_eta_v_phi_map_Barrel4,"All Module mapping validation")
    h_eta_v_phi_map_Barrel4.Add(h_eta_v_phi_map_Barrel3)
    h_eta_v_phi_map_Barrel4.Add(h_eta_v_phi_map_Barrel2)
    h_eta_v_phi_map_Barrel4.Add(h_eta_v_phi_map_Barrel1)
    
    for i in range(len(module_tobs)):
        for j in range(len(module_tobs[i])):
            for key in module_tobs[i][j]:
                h_Et.Fill(module_tobs[i][j][key][4],1)
                h_Eta.Fill(module_tobs[i][j][key][2],1)
                h_Phi.Fill(module_tobs[i][j][key][3],1)        

    c11 = createCanvasPads("11")
    h_Et = SetHisto1D(h_Et,"Et [200 MeV scale]")
    DrawCanvas(c11,h_Et, " SAME")
    # c11.SaveAs("Tau_TOBs_Et.pdf")

    
    c12 = createCanvasPads("12")
    h_Eta = SetHisto1D(h_Eta,"#eta [#times10]")
    DrawCanvas(c12,h_Eta, " SAME")
    # c12.SaveAs("Tau_TOBs_Eta.pdf")

    
    c13 = createCanvasPads("13")
    h_Phi = SetHisto1D(h_Phi,"#phi [#times10]")
    DrawCanvas(c13,h_Phi, " SAME")
    # c13.SaveAs("Tau_TOBs_Phi.pdf")


    

    c7 = createCanvasPads("8")
    
    #h_eta_v_phi_map_ET_1.SetLineWidth(2)
    #h_eta_v_phi_map_ET_1.SetLineColor(1)
    #DrawCanvas(c7,h_eta_v_phi_map_ET_2," ")
    #DrawCanvas(c7,h_eta_v_phi_map_ET_1," SAME")
    DrawCanvas(c7,h_eta_v_phi_map_Barrel4, " SAME")
    
    # c7.SaveAs("plots/FPGAAllmapping_validation.png")
    print("Done..")
    input()
    #return


def PrintLM(canvas_,hist,module_tobs):
    canvas_.cd()
    line = TLine()
    
    line.SetLineWidth(2)
    #print(h_eta_v_phi_map_localMAX.GetXaxis().GetNbins(),h_eta_v_phi_map_localMAX.GetYaxis().GetNbins())
    # Draw FPGA borders
    line.SetLineColor(ROOT.kGreen)
    line.DrawLine(-25,0,25,0)
    line.DrawLine(-25,16,25,16)
    line.DrawLine(-25,16*2,25,16*2)
    line.DrawLine(-25,16*3,25,16*3)
    line.DrawLine(-25,16*4,25,16*4)
    line.DrawLine(-25,0,-25,64)
    line.DrawLine(-25+9,0,-25+9,64)
    line.DrawLine(-25+9+8,0,-25+9+8,64)
    line.DrawLine(-25+9+8*2,0,-25+9+8*2,64)
    line.DrawLine(-25+9+8*3,0,-25+9+8*3,64)
    line.DrawLine(-25+9+8*4,0,-25+9+8*4,64)
    line.DrawLine(-25+9*2+8*4,0,-25+9*2+8*4,64)    
    line.SetLineColor(ROOT.kRed)
    add = 0.5
    lowedge = hist.GetXaxis().GetBinLowEdge(1)+1
    for i in range(hist.GetXaxis().GetNbins()):
        for j in range(hist.GetYaxis().GetNbins()):
            line.SetLineColor(ROOT.kGreen)
            skip=True
            if(hist.GetBinContent(i+1,j+1)<0.01): continue
            
            _X_ = float(i+lowedge-0.5)
            _Y_ = float(j+0.5)#h_eta_v_phi_map_localMAX.GetYaxis().GetBinCenter( h_eta_v_phi_map_localMAX.GetYaxis().FindBin(j) )

            for mod in range(len(module_tobs)):
                for fpga in range(len(module_tobs[mod])):  
                    for key in module_tobs[mod][fpga]:
                        if(module_tobs[mod][fpga][key][2]==_X_ and module_tobs[mod][fpga][key][3]==_Y_ and module_tobs[mod][fpga][key][6]!=0): 
                            line.SetLineColor(ROOT.kRed)   
                            skip = False  
            
            if(skip):
                continue
            #print(_X_-add,_Y_-add,_X_+add,_Y_-add)
            #print(_X_-add,_Y_+add,_X_+add,_Y_+add)
            #print(_X_-add,_Y_-add,_X_-add,_Y_+add)
            #print(_X_+add,_Y_-add,_X_+add,_Y_+add)
            #if(h_eta_v_phi_map.GetBinContent(i+1,j+1)>60000): print(h_eta_v_phi_map.GetBinContent(i+1,j+1),_X_,_Y_)

            #print(i,j,_X_,_Y_,h_eta_v_phi_map_localMAX.GetBinContent(i+1,j+1))
            line.DrawLine(_X_-add,_Y_-add,_X_+add,_Y_-add)
            line.DrawLine(_X_-add,_Y_+add,_X_+add,_Y_+add)
            line.DrawLine(_X_-add,_Y_-add,_X_-add,_Y_+add)
            line.DrawLine(_X_+add,_Y_-add,_X_+add,_Y_+add)




    canvas_.Modified()
    canvas_.Update()

def Divide2FPGA(dicc):
    TOBs_dic_FPGA0 = {}
    TOBs_dic_FPGA1 = {}
    TOBs_dic_FPGA2 = {}
    TOBs_dic_FPGA3 = {}
    for key in dicc:
        if(dicc[key][1]==0): TOBs_dic_FPGA0[key]=dicc[key]
        if(dicc[key][1]==1): TOBs_dic_FPGA1[key]=dicc[key]
        if(dicc[key][1]==2): TOBs_dic_FPGA2[key]=dicc[key]
        if(dicc[key][1]==3): TOBs_dic_FPGA3[key]=dicc[key]
        
    TOBs_dic_FPGA0 = dict(sorted(TOBs_dic_FPGA0.items(), reverse=True, key=lambda item: item[1][4]))
    TOBs_dic_FPGA1 = dict(sorted(TOBs_dic_FPGA1.items(), reverse=True, key=lambda item: item[1][4]))
    TOBs_dic_FPGA2 = dict(sorted(TOBs_dic_FPGA2.items(), reverse=True, key=lambda item: item[1][4]))
    TOBs_dic_FPGA3 = dict(sorted(TOBs_dic_FPGA3.items(), reverse=True, key=lambda item: item[1][4]))
    while len(TOBs_dic_FPGA0)>6: TOBs_dic_FPGA0.popitem() 
    while len(TOBs_dic_FPGA1)>6: TOBs_dic_FPGA1.popitem() 
    while len(TOBs_dic_FPGA2)>6: TOBs_dic_FPGA2.popitem() 
    while len(TOBs_dic_FPGA3)>6: TOBs_dic_FPGA3.popitem() 
    return TOBs_dic_FPGA0,TOBs_dic_FPGA1,TOBs_dic_FPGA2,TOBs_dic_FPGA3



def createRatio(h1, h2, color, i):
    h3 = h1.Clone("h3")
    h3.GetXaxis().SetTitle("VBF BDT."+str(i))
    h3.SetLineColor(color)
    h3.SetMarkerStyle(20)
    h3.SetMarkerColor(color)
    h3.SetTitle("")
    #h3.SetMinimum(0.8)
    #h3.SetMaximum(1.35)
    # Set up plot for markers and errors
    h3.Sumw2()
    h3.SetStats(0)
    h3.Divide(h2)
    
    # Adjust y-axis settings
    y = h3.GetYaxis()
    y.SetTitle("train/test")
    y.SetNdivisions(505)
    y.SetTitleSize(20)
    y.SetTitleFont(43)
    y.SetTitleOffset(1.55)
    y.SetLabelFont(43)
    y.SetLabelSize(15)
    
    # Adjust x-axis settings
    x = h3.GetXaxis()
    x.SetTitleSize(20)
    x.SetTitleFont(43)
    x.SetTitleOffset(4.0)
    x.SetLabelFont(43)
    x.SetLabelSize(15)
    MyLowerLimit = 0.0
    MyUpperLimit = 2.0
    h3.GetYaxis().SetRangeUser(MyLowerLimit, MyUpperLimit)
    
    return h3


def createCanvasPads(name_):

    c = TCanvas( name_, 'Dynamic Filling', 1500,1000 )
    c.SetTopMargin(0.07)     
    c.SetRightMargin(0.15)     
    c.SetLeftMargin(0.10)
    
    return c

def SetHisto(h,title):
  h.SetTitle(title)
  # h.GetXaxis().SetTitle("#eta [ "+str(round(h.GetXaxis().GetBinWidth(1),0))+" p/b ]")
  # h.GetYaxis().SetTitle("#phi [ "+str(round(h.GetYaxis().GetBinWidth(1),0))+" p/b ]")
  h.GetXaxis().SetTitle("#eta [ #times10 ]")
  h.GetXaxis().SetTitleOffset(1.0)
  h.GetYaxis().SetTitle("#phi [ #times10  ]")
  h.GetYaxis().SetTitleOffset(1.0)
  h.GetZaxis().SetTitle("Et (scale 200 MeV) [MeV]")
  #h_eta_v_phi_map.GetYaxis().SetNdivisions(0,1,0);
  h.SetLineColor(2)
  h.SetFillStyle(3325)
  h.SetFillColor(2)
  h.SetLineWidth(1)
  h.SetStats(0)
  return h
  
def SetHisto1D (h,title):
  h.SetTitle("")
  # h.GetXaxis().SetTitle("#eta [ "+str(round(h.GetXaxis().GetBinWidth(1),0))+" p/b ]")
  # h.GetYaxis().SetTitle("#phi [ "+str(round(h.GetYaxis().GetBinWidth(1),0))+" p/b ]")
  h.GetXaxis().SetTitle(title)
  h.GetYaxis().SetTitle("counts")
  #h_eta_v_phi_map.GetYaxis().SetNdivisions(0,1,0);
  h.SetLineColor(2)
  h.SetFillStyle(3002)
  h.SetFillColor(2)
  h.SetLineWidth(1)
  # h.SetStats(0)
  gStyle.SetOptStat(10)
  return h

def DrawCanvas(c,h,opt=""):
    c.SetBatch(ROOT.kFALSE)
    c.cd()   
    h.Draw("COLZ"+opt)
    gPad.RedrawAxis()
    c.Modified()
    c.Update()
      


if __name__ == '__main__':
    main()
                                                                                                                       
                 












#xbins = sorted([-4.9,-3.2,-2.7,-2.5,-2.3,-2.1,-1.9,-1.7,-1.5,-1.3,-1.1,-0.9,-0.7,-0.5,-0.3,-0.1,0.1,0.3,0.5,0.7,0.9,1.1,1.3,1.5,1.7,1.9,2.1,2.3,2.5,2.7,3.2,4.9])
#xarray = array('d',xbins)
#hLVL1JetRoIs_eta = TH1F( 'hLVL1JetRoIs_eta', 'Jet RoIs eta;Jet RoI #eta;Number of Jet RoIs', len(xarray)-1, xarray) 
