#!/usr/bin/env python3                                                                                                                                                            
# -*- coding: utf-8 -*-                                                                                                                                                               
import os,sys
import datetime
import glob
date = datetime.datetime.now()
str_date = str(date.year)+"_"+str(date.month)+"_"+str(date.day)

from optparse import OptionParser 
parser = OptionParser(usage = "usage: %prog arguments", version="%prog") 
parser.add_option('-d','--directory1',action="store", type="string", dest='folder_old', help='Main directory of the validation  (default: %default)')
parser.add_option('-c','--compare',action="store_true", dest='compare', help='Makes the coparison if you give "-d" option  (default: %default)')
parser.set_defaults(folder_old="Non_dold")
parser.set_defaults(compare=0)
(options,args) = parser.parse_args()

print("*"*50+" xAOD_TauTOBs.py    "+"*"*50)
os.system("python xAOD_TauTOBs.py    -d "+str_date)
print("*"*50+" xAOD_SRjetsTOBs.py "+"*"*50)
os.system("python xAOD_SRjetsTOBs.py -d "+str_date)
print("*"*50+" xAOD_LRjetsTOBs.py "+"*"*50)
os.system("python xAOD_LRjetsTOBs.py -d "+str_date)
print("*"*50+"  xAOD_MetTOBs.py   "+"*"*50)
os.system("python xAOD_MetTOBs.py    -d "+str_date)
print("*"*50+"  xAOD_SumEtTOBs.py   "+"*"*50)
os.system("python xAOD_SumEtTOBs.py  -d "+str_date)
print("*"*50+" Pileup_plots.py    "+"*"*50)
os.system("python Pileup_plots.py    -d "+str_date)


if(options.compare):
    print("*"*50+" ValidationComparisons.py    "+"*"*41)
    os.system("python ValidationComparisons.py --d_old "+options.folder_old+" --d_new "+str_date)

print("*"*50+" Copying files to EOS "+"*"*48)
print("\tLocation: /eos/user/s/serodrig/www/"+str_date)
os.system("cp -rf "+str_date+" /eos/user/s/serodrig/www/")
print("DONE.")
